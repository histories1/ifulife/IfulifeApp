/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

// tslint:disable-next-line: ordered-imports
import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

import SplashScreen from 'Ifulife/src/screens/Splash';
import MainScreen from 'Ifulife/src/screens/Main';
import MsgScreen from 'Ifulife/src/screens/msg';
import MsgRecord from 'Ifulife/src/screens/msg/Record';
import WebViewModal from 'Ifulife/src/screens/msg/WebViewModal';

import MpgScreen from 'Ifulife/src/screens/mpg';
import CalendarScreen from 'Ifulife/src/screens/calendar';
import GasScreen from 'Ifulife/src/screens/gas';
import GasRecordScreen from 'Ifulife/src/screens/gas/Record';
import OpinionScreen from 'Ifulife/src/screens/opinion';
import OpinionListScreen from 'Ifulife/src/screens/opinion/List';
import OpinionRecordScreen from 'Ifulife/src/screens/opinion/Record';
import DonateScreen from 'Ifulife/src/screens/Donate';
import UtilitiesScreen from 'Ifulife/src/screens/utilities';
import UtilitiesPolicy from "Ifulife/src/screens/utilities/Policy";
import UtilitiesBooking from 'Ifulife/src/screens/utilities/Booking';
import UtilitiesRecord from 'Ifulife/src/screens/utilities/Record';
import ParkScreen from 'Ifulife/src/screens/park';
import CodeScanScreen from 'Ifulife/src/screens/code/Scan';
import CodeShowScreen from 'Ifulife/src/screens/code/Show';
import ProfileScreen from 'Ifulife/src/screens/profile';
import EditPasswordScreen from 'Ifulife/src/screens/profile/EditPassword';
import LoginScreen from 'Ifulife/src/screens/profile/auth/Login';
import PasswordScreen from 'Ifulife/src/screens/profile/auth/Password';
import IdentityScreen from 'Ifulife/src/screens/profile/auth/Identity';
import RegisterScreen from 'Ifulife/src/screens/profile/auth/Register';
import SettingNotificationScreen from 'Ifulife/src/screens/profile/SettingNotification';
import PrivacyScreen from 'Ifulife/src/screens/webpage/Privacy';
import StatementScreen from 'Ifulife/src/screens/webpage/Statement';
import AgreePageScreen from 'Ifulife/src/screens/webpage/AgreePage';


import { LocaleConfig } from 'react-native-calendars';
LocaleConfig.locales['zh-tw'] = {
  monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
  monthNamesShort: ['1月.', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10', '11月', '12月'],
  dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
  dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
  today: '今日'
};
LocaleConfig.defaultLocale = 'zh-tw';

import Config from "react-native-config";
// 使用全域變數配置環境參數
global.config = Config;


const AppNavigator = createStackNavigator({
  // 預設首頁
  Home: {
    screen: MainScreen,
    params: {
      page: 'home',
    },
    navigationOptions: {
      header: null,
    },
  },
  // 最新消息(社區公告)
  Msg: {
    screen: MsgScreen,
    params: {
      page: 'message',
    },
  },
  // 最新消息列表
  Msg_Record: {
    screen: MsgRecord,
    params: {
      page: 'message_record',
    },
  },
  // 包裹到了
  Mpg: {
    screen: MpgScreen,
    params: {
      page: 'package',
    },
  },
  // 行事曆
  Calendar: {
    screen: CalendarScreen,
    params: {
      page: 'calendar',
    },
  },
  // 瓦斯抄錶
  Gas: {
    screen: GasScreen,
    params: {
      page: 'gas',
    },
  },
  Gas_Record: {
    screen: GasRecordScreen,
    params: {
      page: 'gas_record',
    },
  },
  // 意見提出
  Opinion: {
    screen: OpinionScreen,
    params: {
      page: 'opinion',
    },
  },
  Opinion_List: {
    screen: OpinionListScreen,
    params: {
      page: 'opinionn_list',
    }
  },
  Opinion_Record: {
    screen: OpinionRecordScreen,
    params: {
      page: 'opinion_record',
    },
  },
  // 百元捐款(我要做愛心)
  Donate: {
    screen: DonateScreen,
    params: {
      page: 'donate',
    },
  },
  // 公設預約
  Utilities: {
    screen: UtilitiesScreen,
    params: {
      page: 'utilities',
    },
  },
  Utilities_Booking: {
    screen: UtilitiesBooking,
    params: {
      page: 'utilities_booking',
    }
  },
  Utilities_Record: {
    screen: UtilitiesRecord,
    params: {
      page: 'utilities_record',
    },
  },
  // 車位共享
  Park: {
    screen: ParkScreen,
    params: {
      page: 'park',
    },
  },
  // 掃碼
  Codescan: {
    screen: CodeScanScreen,
    params: {
      page: 'codescan',
    },
  },
  Codeshow: {
    screen: CodeShowScreen,
    params: {
      page: 'codescan',
    },
  },
  // 我的（預設畫面）
  Profile: {
    screen: ProfileScreen,
    params: {
      page: 'profile',
    },
  },
  // 開通
  Profile_Identity: {
    screen: IdentityScreen,
    params: {
      page: 'identity',
    },
  },
  Profile_Register: {
    screen: RegisterScreen,
    params: {
      page: 'register',
    },
  },
  // 登入
  Profile_Login: {
    screen: LoginScreen,
    params: {
      page: 'login',
    },
  },
  // 修改我的密碼
  Profile_EditPassword: {
    screen: EditPasswordScreen,
    params: {
      page: 'password',
    },
  },
  // 推播設定
  Setting_Notification: {
    screen: SettingNotificationScreen,
    params: {
      page: 'password',
    },
  },

}, {
    initialRouteName: 'Home'
});

const RootStack = createStackNavigator(
  {
    Main: {
      screen: AppNavigator,
    },
    Msg_Record_Modal: {
      screen: WebViewModal,
      params: {
        page: 'message_record_modal',
      },
    },
    Utilities_Policy: {
      screen: UtilitiesPolicy,
      params: {
        page: 'utilities_policy',
      }
    },
    Privacy: {
      screen: PrivacyScreen,
      params: {
        page: 'webpage_privacy',
      }
    },
    Statement: {
      screen: StatementScreen,
      params: {
        page: 'webpage_statement',
      }
    },
    AgreePage: {
      screen: AgreePageScreen,
      params: {
        page: 'webpage_agreepage',
      }
    }
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
);

const InitialNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  Root: RootStack
});


export default createAppContainer(InitialNavigator);
