/**
 * TODO : 加入清除storage後呼救API後端紀錄
 * TODO : 改用 [async-storage/API.md at LEGACY · react-native-community/async-storage](https://github.com/react-native-community/async-storage/blob/LEGACY/docs/API.md#useAsyncStorage)方式取代目前作法
 */

import AsyncStorage from '@react-native-community/async-storage';
import Storage from 'react-native-storage';

const PKstorage = new Storage({
  // 最大容量
  size: 1000,
  storageBackend: AsyncStorage,
  defaultExpires: 1000 * 3600 * 24,
  enableCache: true,
  // 你可以在构造函数这里就写好sync的方法 // 或是在任何时候，直接对storage.sync进行赋值修改 // 或是写到另一个文件里，这里require引入
  // 如果storage中没有相应数据，或数据已过期，
  // sync: require('可寫另外一隻程式專門處理sync機制'),主要返回最新的儲存資訊
  sync: {}
});
global.storage = PKstorage;

// store
const setMember = (data:object, expireSec:number) => {
  global.storage.save({
    key: '@ifulife:member',
    data,
    expires: (typeof expireSec === 'number') ? 1000 * expireSec : null,
  });
  // console.log(data);
}

const getMember = ( navigation ) => {
  return new Promise((resolve, reject) => {
    global.storage.load({
      key: '@ifulife:member',
      // syncInBackground: false
    }).then( ret => {
      resolve(ret);
      //notice : 加入可測試驗證逾期配置是否正常除錯程式碼
      // if (global.storage.cache["@ifulife:member"]) {
      //   let timestamp = global.storage.cache["@ifulife:member"].expires;
      //   var time = new Date(timestamp);
        // console.log('逾期時間:', time.toString());
      // }
    }).catch(err => {
      /**
       * @todo 後期紀錄逾期狀態！
       */
      switch (err.name) {
        // '尚未登入狀態/無法使用暫存檔案儲存登入資訊'
        case 'NotFoundError':
          // console.log(err.name);
          // reject(err.message);
          reject(null);
          break;
        case 'ExpiredError':
          //notice : 加入可測試驗證逾期配置是否正常除錯程式碼
          // let timestamp = global.storage.cache["@ifulife:member"].expires;
          // var time = new Date(timestamp);
          // console.log('逾期！？:', time.toString());
          // reject(undefined);
          // return false;
          if (navigation ) {
            // console.log('layout footer: ', navigation.state);
            if( navigation.state.routeName != "Home" ){
              alert('登入資訊已經逾期，請重新登入！');
              navigation.navigate('Profile_Login');
            }else{
              reject(null);
            }
          }else{
            reject(null);
          }
          break;
        default:
          reject('Default Unknown Error');
          break;
      }
    })
  });
}

const removeMember = () => {
  global.storage.remove({
    key: '@ifulife:member'
  });
}


export default {
  setMember,
  getMember,
  removeMember
};