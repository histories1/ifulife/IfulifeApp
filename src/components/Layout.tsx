import React, { Component } from 'react';
import {
  Alert,
  Platform,
  View,
  Keyboard
} from 'react-native';
import {
  Container,
  Text,
  Footer,
  FooterTab,
  Button,
  Badge,
  Spinner,
  Icon
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';

import { withNavigation } from 'react-navigation';

import styles from 'Ifulife/src/constants/newstyles';

const options = {
  mediaType: "photo",
  maxWidth: 768,
  maxHeight: 640,
  quality: 1,
  permissionDenied: {
    title: "未授權程式存取相機",
    text: "您並未授權應用程式使用您的裝置相機功能，若您打算開啟，請至設定中更改。",
    reTryTitle: "重試",
    okTitle: "確定"
  }
}
class FooterComponent extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      showFooter: true
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  _keyboardDidShow() {
    this.setState({ showFooter: false });
  }
  _keyboardDidHide() {
    this.setState({ showFooter: true });
  }

  render(){
    const { active, member, setHeight, navigation } = this.props;
    const height = (setHeight) ? setHeight : 70;

    const ftIcons = [
      {
        label: '首頁',
        icon: (<Icon type="Entypo" name="home" size={28} style={{ color: (active === '首頁') ? EStyleSheet.value('$colorMainText') : '#828282'}} />),
        goto: 'Home'
      },
      {
        label: '掃描',
        icon: (<Icon type="AntDesign" name="scan1" size={28} style={{ color: (active === '掃描') ? EStyleSheet.value('$colorMainText') : '#828282'}} />),
        goto: 'Codescan'
      },
      {
        label: '條碼',
        icon: (<Icon type="FontAwesome" name="qrcode" size={28} style={{ color: (active === '條碼') ? EStyleSheet.value('$colorMainText') : '#828282'}} />),
        goto: 'Codeshow'
      },
      {
        label: '我的帳號',
        icon: (<Icon type="FontAwesome5" name="user" size={28} style={{ color: (active === '我的帳號') ? EStyleSheet.value('$colorMainText') : '#828282'}} />),
        goto: 'Profile'
      }
    ];

    if( Platform.OS == "android" && !this.state.showFooter ){
      return (<View />)
    }

    return (
      <Footer style={{ height: height, }}>
        <FooterTab style={{ backgroundColor: 'white' }}>
          {
            ftIcons.map(function(icon, key){
              let buttonStyle=undefined;
              let textColor=undefined;
              if( active == icon.label ){
                buttonStyle = { borderTopColor: '#FAC000', borderTopWidth: 3, borderRadius: 0,};
                textColor = { color: EStyleSheet.value('$colorMainText')};
              }

              return (
              <Button key={key} vertical style={[buttonStyle]}
                onPress={async () => {
                  if( icon.label == "條碼") {
                    if(!member) {
                      Alert.alert(
                        '您必須登入才能顯示專屬條碼',
                        '是否前往登入？',
                        [
                          {
                            text: '取消',
                            onPress: () => { },
                            style: 'cancel',
                          },
                          { text: '前往', onPress: () => { navigation.navigate('Profile_Login'); } },
                        ],
                        { cancelable: false },
                      );
                    } else {
                      navigation.navigate(icon.goto, {
                        member: member
                      });
                    }
                  }else if( icon.label == "我的帳號" ){
                    navigation.navigate(icon.goto, {
                      isSignin: (member) ? true : false
                    });
                  }else{
                    navigation.navigate(icon.goto);
                  }
                }}
              >
                {icon.icon}
                <Text style={textColor}>{icon.label}</Text>
              </Button>
              )
            })
          }
        </FooterTab>
      </Footer>
    )
  }
}
const IFooter = withNavigation(FooterComponent)

class PageLoading extends Component {
  render() {
    return (
      <Container style={styles.center}>
        <Spinner color={EStyleSheet.value('$colorSubem')} />
        <Text style={{ color: EStyleSheet.value('$colorMainText') }}>載入中..</Text>
      </Container>
    )
  }
}

export default {
  IFooter,
  PageLoading
}