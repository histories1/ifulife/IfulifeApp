// 公益捐款畫面

import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import {
  Container,
  Body,
} from 'native-base';
import Device from 'react-native-detect-device';
import EStyleSheet from 'react-native-extended-stylesheet';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';


export default class Profilecreen extends Component {
  static navigationOptions = {
    title: '百元捐款',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white',
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true
    }
  }

  protected hideSpinner() {
    this.setState({ isLoading: false });
  }

  protected render() {
    return (
      <Container>
        <Body style={{ flex: 1 }}>
          <WebView
            onLoad={() => this.hideSpinner()}
            style={{ width: Device.width, height: Device.height }}
            source={{ uri: 'https://donate.spgateway.com/ShanShui/Happiness' }}
            tartInLoadingState={true}
            domStorageEnabled={true}
            javaScriptEnabled={true}
            automaticallyAdjustContentInsets={true}
          />
          {this.state.isLoading && (<L.PageLoading />)}
        </Body>

        {<L.IFooter style={styles.colorDonate2} />}
      </Container>
    );
  }
}
