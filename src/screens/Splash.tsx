import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';
import styles from 'Ifulife/src/constants/newstyles';
export default class SplashScreen extends Component<Props> {

  protected performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    )
  }

  protected async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.props.navigation.navigate('Root');
    }
  }

  protected render() {
    let dimensions = Dimensions.get("window");

    return (
      <View style={[styles.container, {backgroundColor:'white'}]}>
        <ResponsiveImage
          initWidth={dimensions.width}
          initHeight={dimensions.height}
          source={require('Ifulife/src/assets/splashScreen.png')}
          resizeMode="contain"
        />
      </View>
    );
  }
}