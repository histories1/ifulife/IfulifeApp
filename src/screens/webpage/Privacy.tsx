// 公益捐款畫面

import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-native';
import { WebView } from 'react-native-webview';
import {
  Fab,
  Icon
} from 'native-base';
import Config from "react-native-config";
import Device from 'react-native-detect-device';
import EStyleSheet from 'react-native-extended-stylesheet';
import L from 'Ifulife/src/components/Layout';

export default class PrivacyScreen extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true
    }
  }

  protected hideSpinner() {
    this.setState({ isLoading: false });
  }

  protected render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          onLoad={() => this.hideSpinner()}
          style={{ width: Device.width, height: Device.height }}
          source={{ uri: Config.API_URL+'/privacy' }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={true}
          automaticallyAdjustContentInsets={true}
        />
        {this.state.isLoading && (<L.PageLoading />)}

        <Fab
          style={{ backgroundColor: EStyleSheet.value('$colorSubem') }}
          position="bottomRight"
          onPress={() => this.props.navigation.goBack()}>
          <Icon type="FontAwesome" size={16} name="close" style={{ color: 'white' }} />
        </Fab>
      </SafeAreaView>
    );
  }
}
