// 公益捐款畫面

import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-native';
import { WebView } from 'react-native-webview';
import {
  Header,
  Segment,
  Body,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Icon
} from 'native-base';
import Config from "react-native-config";
import Device from 'react-native-detect-device';
import L from 'Ifulife/src/components/Layout';

export default class AgreePageScreen extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      activePage: 1,
    }
  }

  selectComponent = (activePage) => () => this.setState({ activePage })

  componentDidMount() {
    this.setState({ isLoading: false });
  }

  _renderComponent = () => {
    if (this.state.activePage === 1) {
      return (
        <WebView
          style={{ width: Device.width, height: 5000 }}
          source={{ uri: Config.API_URL+'/privacy' }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={true}
          automaticallyAdjustContentInsets={true}
        />
      )
    } else if (this.state.activePage === 2) {
      return (<WebView
        style={{ width: Device.width, height: 2500 }}
        source={{ uri: Config.API_URL+'/agreement' }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        automaticallyAdjustContentInsets={true}
      />)
    }
  }

  protected render() {
    if (this.state.isLoading ){
      return <L.PageLoading />
    }else{
      const member = this.props.navigation.getParam('member');

      return (
        <SafeAreaView style={{ flex: 1 }}>
          <Header hasSegment>
            <Body>
              <Segment>
                <Button first
                  active={this.state.activePage === 1}
                  onPress={this.selectComponent(1)}
                ><Text>會員條款</Text></Button>
                <Button last
                  active={this.state.activePage === 2}
                  onPress={this.selectComponent(2)}
                ><Text>隱私政策</Text></Button>
              </Segment>
            </Body>
          </Header>
          <Content>
            {this._renderComponent()}
          </Content>
          <Footer style={{ height: 50 }}>
            <FooterTab style={{ backgroundColor: 'white' }}>
              <Button bordered dark
                onPress={() => {
                  this.props.navigation.goBack();
                }}
              >
                <Text>取消</Text>
              </Button>
              <Button bordered primary
                onPress={() => {
                  this.props.navigation.navigate('Profile_Register', {
                    member
                  });
                }}
              >
                <Text>同意</Text>
              </Button>
            </FooterTab>
          </Footer>

        </SafeAreaView>
      );
    }
  }
}
