// 行事曆列表模式

import React, { Component } from 'react';
import { withNavigation } from 'react-navigation'
import {
  Platform,
  View,
  Text,
  TouchableOpacity,
  Button
} from 'react-native';
import { create } from 'apisauce';
import Config from "react-native-config";
import _ from 'lodash';
import moment from "moment";
import { ExpandableCalendar, AgendaList, CalendarProvider } from 'react-native-calendars';
import L from 'Ifulife/src/components/Layout';

import styles from 'Ifulife/src/constants/newstyles';

class CalendarListTab extends Component<Props> {

  constructor(props) {
    super(props);

    this.state = {
      listitems: undefined,
      displayFirstDay: undefined,
      isLoading : true,
    };
  }

  async componentDidMount() {
    let listitems = [];
    let displayFirstDay = 0;
    try {
      const { clickDay } = this.props;
      displayFirstDay = parseInt(moment(this.props.clickDay, 'YYYY-MM-DD').format('d'));
      // console.log(clickDay);
      listitems = await this.getNowWeekItems(clickDay);
    } finally {
      await this.setState({
        listitems,
        displayFirstDay,
        isLoading: false
      });
    }
  }

  //notice : 每次
  onDateChanged = async (date, updateSource) => {
    let listitems = [];
    let displayFirstDay = 0;
    try {
      console.log('chage start date: ', date);
      displayFirstDay = parseInt(moment(date, 'YYYY-MM-DD').format('d'));
      // console.log('changed updateSource: ', updateSource);
      listitems = await this.getNowWeekItems(date);
      // console.warn('ExpandableCalendarScreen onDateChanged: ', date, updateSource);
    } finally {
      await this.setState({
        listitems,
        displayFirstDay,
        isLoading: false,
      });
    }
  }

  async getNowWeekItems(startDay) {
    try {
      await this.setState({
        isLoading: true,
      });

      const { cmtId } = this.props;

      const api = create({
        baseURL: Config.API_URL,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });
      const listRes = await api.get('/msg/calendarlist/' + cmtId + '/' + startDay);
      // console.log('calendar result: ', listRes);
      // return ;
      if (listRes.status !== 200) {
        // console.log('calendar get: ', listRes);
        const cerr = new Error(listRes.errMsg);
        cerr.code = listRes.code;
        throw cerr;
      }
      // listitems = listRes.data;
      return listRes.data;
      // console.log('HERE', listitems);
    } catch (err) {
      console.log('Error occure: ', err);
      return false;
    }
  }


  getSections() {
    const { listitems } = this.state;
    const sections = _.compact(_.map(listitems, (item) => {
      return { title: item.title, data: item.data };
    }));
    return sections;
  }

  renderEmptyItem() {
    return (
      <View style={styles.emptyItem}>
        <Text style={styles.emptyItemText}>沒有活動</Text>
      </View>
    );
  }

  getMarkedDates = () => {
    const { listitems } = this.state;
    let marked = {};
    listitems.forEach(item => {
      // console.log('item項目: ', item.title);
      marked[item.title] = { marked: true };
    });
    return marked;
  }

  getTheme = () => {
    const themeColor = '#0059ff';
    const lightThemeColor = '#e6efff';
    const disabledColor = '#a6acb1';
    const black = '#20303c';
    const white = '#ffffff';

    return {
      // arrows
      arrowColor: black,
      arrowStyle: { padding: 0 },
      // month
      monthTextColor: black,
      textMonthFontSize: 16,
      textMonthFontFamily: 'HelveticaNeue',
      textMonthFontWeight: 'bold',
      // day names
      textSectionTitleColor: black,
      textDayHeaderFontSize: 12,
      textDayHeaderFontFamily: 'HelveticaNeue',
      textDayHeaderFontWeight: 'normal',
      // today
      todayBackgroundColor: lightThemeColor,
      todayTextColor: themeColor,
      // dates
      dayTextColor: themeColor,
      textDayFontSize: 18,
      textDayFontFamily: 'HelveticaNeue',
      textDayFontWeight: '500',
      textDayStyle: { marginTop: Platform.OS === 'android' ? 2 : 4 },
      // selected date
      selectedDayBackgroundColor: themeColor,
      selectedDayTextColor: white,
      // disabled date
      textDisabledColor: disabledColor,
      // dot (marked date)
      dotColor: themeColor,
      selectedDotColor: white,
      disabledDotColor: disabledColor,
      dotStyle: { marginTop: -2 },
    };
  }


  renderItem = ({ item }) => {
    if (_.isEmpty(item)) {
      return this.renderEmptyItem();
    }

    const id = item.id;
    const headerTitle = item.typeLabel;
    const { memberId, navigation } = this.props;

    const props = {
      typeSlabel: item.typeSlabel,
      duration: item.duration,
      title: item.title,
      button: {
        label: '查看', onPress: () => {
          navigation.navigate('Msg_Record', {
            memberId,
            msgId: id,
            headerTitle: headerTitle
          });
        }
      }
    };

    return (
      <TouchableOpacity
        onPress={props.onPress}
        style={styles.item}
      >
        <View>
          <Text style={styles.itemHourText}>{props.typeSlabel}</Text>
          <Text style={styles.itemDurationText}>{props.duration}</Text>
        </View>
        <Text numberOfLines={2} style={styles.itemTitleText}>{props.title}</Text>
        <View style={styles.itemButtonContainer}>
          <Button title={props.button.label} onPress={props.button.onPress} />
        </View>
      </TouchableOpacity>
    );
  }

  render() {

    //notice : 設定item，每次section變動重新整理
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <CalendarProvider date={this.props.clickDay} onDateChanged={this.onDateChanged}>
          {this.state.listitems &&
            <ExpandableCalendar
              // hideArrows
              // disablePan
              // hideKnob
              // initialPosition={'open'} // ExpandableCalendar.positions.OPEN - can't find static positions
              firstDay={this.state.displayFirstDay}
              markedDates={this.getMarkedDates()}
              calendarStyle={{ paddingLeft: 20, paddingRight: 20 }}
              theme={this.getTheme()}
              leftArrowImageSource={require('../../assets/calendar/previous.png')}
              rightArrowImageSource={require('../../assets/calendar/next.png')}
            />
          }
          {this.state.listitems &&
            <AgendaList
              data={this.state.listitems}
              renderItem={this.renderItem}
              sections={this.getSections()}
            // sectionStyle={{backgroundColor: '#f0f4f7', color: '#79838a'}}
            />
          }
        </CalendarProvider>
      )
    }
  }
}
const ListMode = withNavigation(CalendarListTab);
export default ListMode;
