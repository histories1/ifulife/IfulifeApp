import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import {
  Content,
  Button,
  Text,
} from 'native-base';
import { CalendarList, LocaleConfig } from 'react-native-calendars';
import moment from 'moment';

LocaleConfig.locales['zh-tw'] = {
  monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
  monthNamesShort: ['1月.', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10', '11月', '12月'],
  dayNames: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'],
  dayNamesShort: ['一', '二', '三', '四', '五', '六', '日'],
  today: '今日'
};
LocaleConfig.defaultLocale = 'zh-tw';


const tempStyle = {
  container: {
    backgroundColor: 'green',
  },
  text: {
    color: 'white',
    fontWeight: 'bold'
  },
};
class MonthMode extends Component {

  constructor(props) {
    super(props)

    let now = new Date();
    this.state = {
      eventSource: [],
      //顯示當時日期
      todayFormat: moment(now).format('YYYY-MM-DD'),
    }
  }


  render() {
    const { markedDates, ondayPress } = this.props;
    /**
     * 以下為測試標示事件範本
     */
    // console.log('pass events: ',markedDates);
    // const msgType1 = { key: 'type1', color: 'red' };
    // const msgType2 = { key: 'type2', color: 'blue' };
    // const msgType3 = { key: 'type3', color: 'green' };
    // let demoData = {
    //   '2019-06-25': {
    //     dots: [msgType1, msgType2],
    //     marked: true
    //   },
    //   '2019-06-26': {
    //     dots: [msgType2, msgType3],
    //     marked: true
    //   }
    // };
    // console.log('demo events: ', demoData);

    return (
      <CalendarList
        current={this.state.todayFormat}
        pastScrollRange={3}
        futureScrollRange={2}
        showScrollIndicator={true}
        monthFormat={'yyyy年MM月'}
        markingType={'custom'}
        markedDates={markedDates}
        markingType={'multi-dot'}
        // 若點選到有事件的日期，可切換到列表模式直接檢視該日
        onDayLongPress={(day) => {
          if( markedDates[day.dateString] ){
            ondayPress(day.dateString);
          }
          // console.log('selected day', day)
        }}

      />
    )
  }
}
const CalendarMode = withNavigation(MonthMode);
export default CalendarMode;
