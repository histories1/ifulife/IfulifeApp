// 行事曆

import React, { Component } from 'react';
import {
  Container,
  Tab,
  Tabs,
} from 'native-base';
import Device from 'react-native-detect-device';
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import moment from 'moment';
import { create } from 'apisauce';
import Config from "react-native-config";
import CalendarMode from "./CalendarMode";
import ListMode from './ListMode';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';
import { thisTypeAnnotation } from '@babel/types';

export default class CalendarSecreen extends Component {
  static navigationOptions = ({
    title: '行事曆',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white'
  });

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      events: undefined,
      defaultTab: 0,
      clickDay: undefined
    }
  }


  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        this.setState({
          member: undefined,
          events: undefined,
          clickDay: undefined,
          isLoading: true
        });
      }
    );

    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        //notice : 以當日取得月份往前推3個月取第一天後推2個月取最後一天
        let now = new Date();
        this.setState({ clickDay: moment(now).format('YYYY-MM-DD') });
        const rangeStart = moment(now).subtract(3, 'month').startOf('month').format('YYYY-MM-DD');
        const rangeEnd = moment(now).add(2, 'month').endOf('month').format('YYYY-MM-DD');
        const rangeDate = `${rangeStart}_${rangeEnd}`;
        // let rangeDate = '2019-06-01_2019-06-30';
        let eventsRes = undefined;

        try {
          member = await ExStorage.getMember(this.props.navigation);
          // console.log(member);
          const api = create({
            baseURL: Config.API_URL,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          });

          eventsRes = await api.get('/msg/calendar/' + member.cmtId + '/' + rangeDate);
          // console.log('calendar result: ', eventsRes);
          // return ;
          if (eventsRes.status !== 200) {
            // console.log('calendar get: ', eventsRes);
            let cerr = new Error(eventsRes.errMsg);
            cerr.code = eventsRes.code;
            throw cerr;
          }
          events = eventsRes.data;
        } catch (err) {
          console.log('Error occure: ', err);
        } finally {
          let a = await this.setState({
            member,
            events,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.events);
      }
    );
  }

  switchListMode(dateString) {
    this.setState({ clickDay: dateString});
    this.refs.caltabs.goToPage(1);
  }

  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <Tabs ref="caltabs" initialPage={this.state.defaultTab}
                tabBarUnderlineStyle={{ backgroundColor: EStyleSheet.value('$colorSubem') }} >
            <Tab heading="列表模式"
                 tabStyle={{ backgroundColor: 'white' }}
                 textStyle={{ color: '#000' }}
                 activeTabStyle={{ backgroundColor: 'white', border: 'none' }}
                 activeTextStyle={{ color: EStyleSheet.value('$colorMainBackground') }}>
              <ListMode memberId={this.state.member.memberId} cmtId={this.state.member.cmtId} clickDay={this.state.clickDay} />
            </Tab>
            <Tab heading="月曆模式"
                 tabStyle={{ backgroundColor: 'white' }}
                 textStyle={{ color: '#000' }}
                 activeTabStyle={{ backgroundColor: 'white' }}
                 activeTextStyle={{ color: EStyleSheet.value('$colorMainBackground') }}>
              <CalendarMode markedDates={this.state.events} ondayPress={this.switchListMode.bind(this)} />
            </Tab>
          </Tabs>

          {<L.IFooter member={this.state.member} setHeight={(Device.isIos) ? 24 : 48} />}
        </Container>
      );
    }
  }
}