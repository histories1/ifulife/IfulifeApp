// 包裹到了

import React, { Component } from 'react';
import { View } from 'react-native'
import {
  Container,
  Content,
  List,
  Tab,
  Tabs,
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import { create } from 'apisauce';
import Config from "react-native-config";
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';
import CardBlock from './CardBlock';

export default class MpgSecreen extends Component {
  static navigationOptions = ({
    title: '包裹到了',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white'
  });

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      mpgs: undefined,
    }

    this._clear = this._clear.bind(this);
    this._fetchData = this._fetchData.bind(this);
  }

  async _clear() {
    this.setState({
      member: undefined,
      mpgs: undefined,
      isLoading: true
    });
    return true;
  }

  async _fetchData() {
    let member = undefined;
    let mpgRes = undefined;
    try {
      member = await ExStorage.getMember(this.props.navigation);
      /**
       * @todo 加入判斷機制導向登入提示！
       */
      // console.log(member);
      const api = create({
        baseURL: Config.API_URL,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });

      mpgRes = await api.get('/mpg/list/' + member.unitId + '/' + member.id);
      // console.log('mpg get: ',mpgRes.data);
      if (mpgRes.status !== 200) {
        let cerr = new Error(mpgRes.errMsg);
        cerr.code = mpgRes.code;
        throw cerr;
      }
      mpgs = mpgRes.data;
    } catch (err) {
      if (!err) {
        member = err;
      } else {
        console.warn('reject others: ', err);
      }
    } finally {
      await this.setState({
        member,
        mpgs,
        isLoading: false,
      });
      return true;
    }
  }

  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        await this._clear();
      }
    );

    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        this._fetchData();
      }
    );
  }

  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      const { member } = this.state;

      return (
        <Container>
          <Content>

            <Tabs initialPage={0} tabBarUnderlineStyle={{ backgroundColor: EStyleSheet.value('$colorSubem') }}>
              <Tab heading="未領件"
                   tabStyle={{ backgroundColor: 'white' }}
                   textStyle={{ color: '#000' }}
                   activeTabStyle={{ backgroundColor: 'white' }}
                   activeTextStyle={{ color: EStyleSheet.value('$colorMainBackground') }}>
                {this.state.mpgs.notyet &&
                  <Content padder>
                    <List>
                    {
                      this.state.mpgs.notyet.map((item, key) => {
                        return <CardBlock key={key} item={item} member={member} clear={this._clear} refetch={this._fetchData} />
                      })
                    }
                    </List>
                  </Content>
                }
              </Tab>
              <Tab heading="已領件"
                tabStyle={{ backgroundColor: 'white' }}
                textStyle={{ color: '#000' }}
                activeTabStyle={{ backgroundColor: 'white' }}
                activeTextStyle={{ color: EStyleSheet.value('$colorMainBackground') }}>
                {this.state.mpgs.took &&
                  <Content padder>
                    <List>
                      {
                        this.state.mpgs.took.map((item, key) => {
                          return <CardBlock key={key} item={item} member={member} clear={this._clear} refetch={this._fetchData} />
                        })
                      }
                    </List>
                  </Content>
                }
              </Tab>
              <Tab heading="退件列表"
                   tabStyle={{ backgroundColor: 'white' }}
                   textStyle={{ color: '#000' }}
                   activeTabStyle={{ backgroundColor: 'white' }}
                   activeTextStyle={{ color: EStyleSheet.value('$colorMainBackground') }}>
                {this.state.mpgs.rejects &&
                  <Content padder>
                    <List>
                      {
                        this.state.mpgs.rejects.map((item, key) => {
                          return <CardBlock key={key} item={item} />
                        })
                      }
                    </List>
                  </Content>
                }
              </Tab>
            </Tabs>

          </Content>

          {<L.IFooter  member={this.state.member} style={styles.colorMpg2} />}
        </Container>
      );
    }
  }
}