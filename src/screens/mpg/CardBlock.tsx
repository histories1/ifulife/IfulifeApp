// 包裹到了項目列表

import React, { Component } from 'react';
import { View, Alert } from "react-native";
import { withNavigation } from "react-navigation";
import {
  ListItem,
  H2,
  Button,
  Text,
  Icon
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { create } from 'apisauce';
import Config from "react-native-config";

class CardBlockComponent extends Component {

  constructor(props) {
    super(props);
  }

  async postReject(mpgId) {
    const { member } = this.props;
    try {
      const api = create({
        baseURL: Config.API_URL,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });
      const jsondata = {
        mpgId: mpgId,
        memberId: member.id,
      };
      mpgRes = await api.post('/mpg/reject', jsondata);
      // console.log('mpg get: ',mpgRes.data);
      if (mpgRes.status !== 200) {
        let cerr = new Error(mpgRes.errMsg);
        cerr.code = mpgRes.code;
        throw cerr;
      }
      Alert.alert(
        '已受理退件申請',
        '請記得將物品放至櫃檯',
        [
          {
            text: '確定', onPress: () => {
              this.props.clear();
              this.props.refetch();
            }
          },
        ],
        { cancelable: false },
      );
    } catch (err) {
      alert('設定退件發生錯誤！');
      console.warn('<',err.name,'>',' Message: ', err.message);
    }
  }


  render() {
    const { item, member } = this.props;
    // console.log(this.props.item);
    let iconname = '';
    if (item.mpgTypeLabel == '包裹') {
      iconname = 'package-variant-closed';
    } else {
      iconname = 'email';
    }

    let Icons = null;
    let showTimeinfo = null;
    let showCode = null;
    // 退件
    if(item.return_apply_at ){
      if( item.return_code ){
        Icons = (
          <View>
            <Icon type="MaterialCommunityIcons" name={iconname} style={{ fontSize: 60 }} />
            <Icon type="MaterialIcons" name="cancel" style={{ color:'#D60303', position: 'absolute', top: 40, right: 10, fontSize: 26 }} />
          </View>
        );
        showTimeinfo = (
          <View>
            <View style={{ flexDirection: 'row',}}>
              <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 18 }}>由</Text>
              <Text style={{ color: '#00629A', fontSize: 14, lineHeight: 18 }}> {item.returnMemberName}</Text>
              <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 18 }}> 於{item.return_apply_at}</Text>
            </View>
            <View>
              <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 18, textAlign: 'left' }}>申請退貨/已完成</Text>
            </View>
          </View>
        );

      }else{
        Icons = (
          <View>
            <Icon type="MaterialCommunityIcons" name={iconname} style={{ fontSize: 60 }} />
            <Icon type="MaterialIcons" name="cancel" style={{ color: '#D60303', position: 'absolute', top: 40, right: 10, fontSize: 26 }} />
          </View>
        );
        showTimeinfo = (
          <View style={{ alignItems: 'flex-start'}}>
            <View style={{ flexDirection: 'row', }}>
              <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 18 }}>由</Text>
              <Text style={{ color:'#00629A', fontSize: 14, textAlign: 'left', lineHeight: 18 }}> {item.returnMemberName}</Text>
              <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 18 }}> 於{item.return_apply_at}</Text>
            </View>
            <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 18, textAlign: 'left' }}>申請退貨/處理中</Text>
          </View>
        );
      }
    // 已領
    }else if( item.collar_at ){
      Icons = (
        <View>
          <Icon type="MaterialCommunityIcons" name={iconname} style={{ fontSize: 60 }} />
          <Icon type="FontAwesome5" name="check-circle" style={{ color: '#00800B', position: 'absolute', top: 40, right: 10, fontSize: 26 }} />
        </View>
      );
      showTimeinfo = (
        <View style={{}}>
          <Text style={{ fontSize: 14, textAlign: 'left', lineHeight: 15 }}>於{item.collar_at}取件</Text>
        </View>
      );
    // 未領
    }else{
      Icons = (
        <View>
          <Icon type="MaterialCommunityIcons" name={iconname} style={{ fontSize: 60 }} />
          <Icon type="FontAwesome5" name="exclamation-circle" style={{ color: '#FF9600', position: 'absolute', top: 40, right: 10, fontSize: 26 }} />
        </View>
      );

      showTimeinfo = <Text numberOfLines={2} style={{ fontSize: 14, textAlign:'left', lineHeight:15 }}>於{item.receipt_at}送達</Text>

      showCode = <Button small bordered style={{ width:80 ,position: 'absolute', bottom: 50, right: -5 }}
        onPress={() => {
          this.props.navigation.navigate('Codeshow', {
            member: member
          });
        }}
      >
        <Icon type="FontAwesome5" name="qrcode" size={14} style={{ marginLeft: 10 }} />
        <Text style={{ textAlign: "left", paddingLeft: 0 }}>領件</Text>
      </Button>
    }

    return (
      <ListItem style={{ minHeight: 160, paddingTop: -10, }}>
        <Grid>
          <Col size={1}>
            {Icons}
          </Col>
          <Col size={4} style={{alignItems: 'flex-start',}}>
            <View style={{flexDirection: 'row',}}>
              <Text style={{ color: '#00629A', fontSize: 16}}>{item.onwerMemberName}</Text>
              <Text style={{ fontSize: 16 }}> 的{item.mpgTypeLabel}</Text>
            </View>
            <View style={{marginTop:5}}>
              {showTimeinfo}
              {item.remark !== "" &&
                <Text style={{ fontSize: 11, textAlign: 'left' }}>{item.remark}</Text>
              }
            </View>
            <View style={{ marginVertical: 3,}}>
              <Text note style={{ fontSize: 12 }}>條碼: {item.barcode}</Text>
            </View>
            {item.logisticsLabel !== "" &&
              <View style={{ marginVertical: 3, }}>
                <Text note style={{ fontSize: 12 }}>物流: {item.logisticsLabel}</Text>
              </View>
            }
            <View style={{ marginVertical: 3, }}>
              <Text note style={{ fontSize: 12 }}>社區編號: {item.mpgcode}</Text>
            </View>

            {showCode}

            {this.props.member &&
              <Button small bordered style={{ width: 80, position: 'absolute', bottom: 10, right: -5 }}
                onPress={() => {
                  // console.log('item :', item);
                  this.postReject(item.id);
                }}
              >
                <Icon type="FontAwesome5" name="share" size={14} style={{ marginLeft: 10 }} />
                <Text style={{ textAlign: "left", paddingLeft: 0 }}>退件</Text>
              </Button>
            }
          </Col>
        </Grid>
      </ListItem>
    )
  }
}
export default CardBlock = withNavigation(CardBlockComponent)