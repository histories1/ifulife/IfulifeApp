// 首頁

import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  Alert,
  Linking,
  TouchableOpacity
} from 'react-native';
import { SafeAreaView, withNavigation } from 'react-navigation';
import {
  Text,
  Button,
  Icon,
  Badge
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import ResponsiveImage from 'react-native-responsive-image';
import Swiper from 'react-native-swiper';
import Device from 'react-native-detect-device';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { create } from 'apisauce';
import Config from "react-native-config";
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';
import MpgIcon from 'Ifulife/src/constants/svgicon/Mpg';
import DonateIcon from 'Ifulife/src/constants/svgicon/Donate';
import ActivityIcon from 'Ifulife/src/constants/svgicon/Activity';
import CalendarIcon from 'Ifulife/src/constants/svgicon/Calendar';
import GasIcon from 'Ifulife/src/constants/svgicon/Gas';
import OpinionIcon from 'Ifulife/src/constants/svgicon/Opinion';
import UtilitiesIcon from 'Ifulife/src/constants/svgicon/Utilities';
import ParkIcon from 'Ifulife/src/constants/svgicon/Park';

export default class MainScreen extends Component<Props> {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      newest: undefined,
      badges: undefined
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener(
      'willFocus', async (payload) => {
        this.setState({
          member: undefined,
          newest: undefined,
          isLoading: true
        });
        // console.log('will focus', member);
      }
    );
    navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let newest = undefined;
        //notice : 為了避免未登入情況會發生錯誤，暫時規則直接指定鼎藏帝景編號(透過jenny戶別！)
        const unitId = 32;
        const api = create({
          baseURL: Config.API_URL,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        });
        let badges = undefined;
        try{
          newestRes = await api.get('/msg/newest/' + unitId);
          if (newestRes.status !== 200) {
            let cerr = new Error(newestRes.errMsg);
            cerr.code = newestRes.code;
            throw cerr;
          }
          newest = newestRes.data;
          //notice : 尚未解決會員登入丟出錯誤正確處理機制！
          member = await ExStorage.getMember(navigation);

          //notice : 臨時處理的載入包裹與最新消息方案
          if( member ){
            badgeRes = await api.get('/mybadge/' + member.cmtId + '/' + member.unitId + '/' + member.id);
            if(badgeRes.status !== 200) {
              let cerr = new Error(badgeRes.errMsg);
              cerr.code = badgeRes.code;
              throw cerr;
            }
            badges = badgeRes.data;
          }
        } catch ( err ){
          console.warn( err );
        } finally {
          this.setState({
            member,
            newest,
            badges,
            isLoading: false
          });
        }
        // console.log('didFocus', payload);
      }
    );
  }

  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {

      let logoSize = (Device.isPhone) ? 80 : 160;

      const { navigation } = this.props;
      const { member, newest, badges } = this.state;

      return (
        <ImageBackground source={require('Ifulife/src/assets/theme/background2x.png')} style={[styles.mainbackground]}>
        <SafeAreaView style={styles.container}>

          <View style={styles.logoBock}>
            <ResponsiveImage
              initWidth={logoSize}
              initHeight={logoSize}
              source={require('Ifulife/src/assets/theme/logo2x.png')}
              resizeMode="contain"
              style={styles.logoSelf}
            />
            <Text style={styles.logoTitle}>愛富生活</Text>
            <Button iconRight rounded style={styles.logoBtn}>
              <Text numberOfLines={1} style={styles.logoBtnTxt}>鼎藏帝景</Text>
                <Icon type="Ionicons" name="ios-arrow-forward" size={20} style={[styles.logoBtnIcon, {color:'white'}]} />
            </Button>
          </View>

          <View style={styles.splinkBlock}>
            <TouchableOpacity onPress={()=>{
              Linking.openURL('https://www.sst.org.tw/index.php/aboutus').catch((err) => console.error('An error occurred', err));
            }}>
              <ResponsiveImage
                initWidth={125}
                initHeight={18}
                source={require('Ifulife/src/assets/theme/foundlink.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>

          <Grid>
            <Row size={1}>
              <View style={[styles.newsBlock]}>
                <Grid>
                  <Row size={2} style={styles.newsHeader}>
                    <Col style={{ flexDirection: 'row', alignItems:'center'}}>
                      <View style={styles.newsIcon}>
                        <Icon type="FontAwesome5" name="bullhorn" size={20} style={{ color:EStyleSheet.value('$colorMainText') }} />
                      </View>
                      <Text style={styles.newsTitle}>最新消息</Text>
                      {badges &&
                        <Badge style={styles.newsBadges}><Text>{badges.msgcount}</Text></Badge>
                      }
                    </Col>
                    <Col style={{ justifyContent: 'center' }}>
                      <Button bordered iconRight style={styles.newsListBtn}
                        onPress={() => {
                          if( !this.state.member) {
                            Alert.alert(
                              '您必須登入才能使用最新消息功能',
                              '是否前往登入？',
                              [
                                {
                                  text: '取消',
                                  onPress: () => { },
                                  style: 'cancel',
                                },
                                { text: '前往', onPress: () => { this.props.navigation.navigate('Profile_Login'); } },
                              ],
                              { cancelable: false },
                            );
                          } else {
                            this.props.navigation.navigate('Msg');
                          }
                        }}
                      >
                        <Text style={{fontSize:12, color:'#979797'}}>看所有消息</Text>
                          <Icon type="EvilIcons" name="arrow-right" size={8}
                              style={{ position: "absolute", top: (Device.isIos) ? 0 : 3, right: -15, color: "#979797"}} />
                      </Button>
                    </Col>
                  </Row>
                  <Row size={3}>
                    {newest &&
                    ( <Swiper loop={true}
                            autoplay={true}
                            autoplayTimeout={4}
                            showsButtons={false}
                            showsPagination={false}>
                              {
                    newest.map(function(item, key){
                      return (
                        <Grid key={key}>
                          <Col size={4}>
                            <View style={{flexDirection: 'row', marginLeft:20, marginVertical: 5,}}>
                              <Text style={{ color: EStyleSheet.value('$colorMainText') }}>{item.createFormat}</Text>
                              <View style={{ backgroundColor:'#34BC00', borderRadius: 30, paddingHorizontal:10, paddingVertical: 5, marginLeft: 10,}}>
                                <Text style={{ fontSize: 10, color: 'white' }}>{item.msgType}</Text>
                              </View>
                            </View>
                            <View style={{marginLeft:20}}>
                              <Text numberOfLines={2} style={{ textAlign: "left", fontSize: 12, lineHeight: 16, color: "#979797" }}>{item.title}</Text>
                            </View>
                          </Col>
                          <Col size={1} style={{justifyContent:'center', alignItems: 'center',}}>
                            <TouchableOpacity style={styles.newsGoBtn} onPress={()=>{
                              if (!member) {
                                Alert.alert(
                                  '您必須登入才能使用最新消息功能',
                                  '是否前往登入？',
                                  [
                                    {
                                      text: '取消',
                                      onPress: () => { },
                                      style: 'cancel',
                                    },
                                    { text: '前往', onPress: () => { navigation.navigate('Profile_Login'); } },
                                  ],
                                  { cancelable: false },
                                );
                              } else {
                                navigation.navigate('Msg_Record', {
                                  memberId: member.id,
                                  msgId: item.id,
                                  headerTitle: item.msgType
                                });
                              }
                            }}>
                              <Icon type="Ionicons" name="ios-arrow-forward" size={50} style={{ color: "gray" }} />
                              <Text style={{ fontSize: 14, color: '#979797', paddingLeft:5 }}>看內容</Text>
                            </TouchableOpacity>
                          </Col>
                        </Grid>
                      )
                    })
                              }
                    </Swiper> )
                  }
                  </Row>

                </Grid>
              </View>
            </Row>
            <Row size={2}>
              <Grid>
                <Row>
                  <Col>
                    <NavMenu member={this.state.member} item={{
                      label: '包裹到了',
                      goto: 'Mpg',
                      exstyle: styles.menuLeft,
                      icon: (<MpgIcon width={40} height={40} />),
                      badge: (badges) ? badges.mpgcount : undefined,
                      needSignin: true,
                      isOpen: true
                    }} />
                  </Col>
                  <Col>
                    <NavMenu member={this.state.member} item={{
                      label: '行事曆',
                      goto: 'Calendar',
                      exstyle: styles.menuRight,
                      icon: (<CalendarIcon width={40} height={40} />),
                      needSignin: true,
                      isOpen: true
                    }} />
                  </Col>
                </Row>
                  <Row>
                    <Col>
                      <NavMenu member={this.state.member} item={{
                        label: '意見提出',
                        goto: 'Opinion',
                        exstyle: styles.menuLeft,
                        icon: (<OpinionIcon width={40} height={40} />),
                        needSignin: true,
                        isOpen: true
                      }} />
                    </Col>
                    <Col>
                      <NavMenu member={this.state.member} item={{
                        label: '瓦斯抄錶',
                        goto: 'Gas',
                        exstyle: styles.menuRight,
                        icon: (<GasIcon width={40} height={40} />),
                        needSignin: true,
                        isOpen: true
                      }} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <NavMenu member={this.state.member} item={{
                        label: '課程報名',
                        goto: 'activity',
                        exstyle: styles.menuLeft,
                        icon: (<ActivityIcon width={40} height={40} />),
                        needSignin: true,
                        isOpen: false
                      }} />
                    </Col>
                    <Col>
                      <NavMenu member={this.state.member} item={{
                        label: '百元捐款',
                        goto: 'Donate',
                        exstyle: styles.menuRight,
                        icon: (<DonateIcon width={40} height={40} />),
                        needSignin: false,
                        isOpen: true
                      }} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <NavMenu member={this.state.member} item={{
                        label: '公設預約',
                        goto: 'Utilities',
                        exstyle: styles.menuLeft,
                        icon: (<UtilitiesIcon width={40} height={40} />),
                        needSignin: true,
                        isOpen: false
                      }} />
                    </Col>
                    <Col>
                      <NavMenu member={this.state.member} item={{
                        label: '車位共享',
                        goto: 'Park',
                        exstyle: styles.menuRight,
                        icon: (<ParkIcon width={40} height={40} />),
                        needSignin: true,
                        isOpen: false}} />
                    </Col>
                  </Row>
              </Grid>
            </Row>
          </Grid>

          {<L.IFooter active="首頁" setHeight={(Device.isIos) ? 28 : 52} member={this.state.member} />}

        </SafeAreaView>
        </ImageBackground>
      );
    }
  }
}

// tslint:disable-next-line: max-classes-per-file
class MenuButton extends Component<Props> {
  constructor(props) {
    super(props)
  }

  render() {
    const { item, member } = this.props;
    //notice : 當包裹數量為0要改成undefined型態否則會出現Error
    if (item.badge === 0 ){
      item.badge=undefined;
    }

    return (
      <TouchableOpacity style={[styles.menuItem, item.exstyle]}
        onPress={() => {
            if (!item.isOpen) {
              alert(item.label + '功能尚未啟用喔');
            } else if (item.needSignin && !member) {
              Alert.alert(
                '您必須登入才能使用' + item.label + '功能',
                '是否前往登入？',
                [
                  {
                    text: '取消',
                    onPress: () => { },
                    style: 'cancel',
                  },
                  { text: '前往', onPress: () => { this.props.navigation.navigate('Profile_Login'); } },
                ],
                { cancelable: false },
              );
            } else {
              this.props.navigation.navigate(item.goto)
            }
          }
        }
      >
        <View style={styles.menuIcon}>{item.icon}</View>
        <Text>{item.label}</Text>
        {item.badge &&
          <Badge style={styles.menuBadge}><Text>{item.badge}</Text></Badge>
        }
      </TouchableOpacity>
    )
  }
}
const NavMenu = withNavigation(MenuButton);