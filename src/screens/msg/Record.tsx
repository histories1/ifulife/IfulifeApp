// 社區公告訊息內容

import React, { Component } from 'react';
import {
  TouchableOpacity,
  Linking,
  SafeAreaView
} from 'react-native';
import { WebView }from 'react-native-webview';
import {
  Root,
  Container,
  Content,
  Left,
  Right,
  Body,
  H2,
  Button,
  Text,
  Icon,
  Card,
  CardItem
} from 'native-base';
import ResponsiveImage from 'react-native-responsive-image';
import { create } from 'apisauce';
import Config from 'react-native-config';
import EStyleSheet from 'react-native-extended-stylesheet';
import Device from 'react-native-detect-device';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';
import * as AddCalendarEvent from 'react-native-add-calendar-event';

// export default
export default class RecordScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: EStyleSheet.value('$colorMainBackground')
      },
      title: navigation.getParam('headerTitle'),
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      msgRecord: undefined,
      carditemWidth: null
    }
  }

  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        this.setState({
          isLoading: true,
          msgRecord: undefined,
          carditemWidth: null,
          eventConfig: undefined
        });
      }
    );

    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let msgRes = undefined;
        let eventConfig = undefined;

        const { navigation } = this.props;
        const msgId = navigation.getParam('msgId');
        const memberId = navigation.getParam('memberId');
        const api = create({
          baseURL: Config.API_URL,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        });

        try {
          msgRes = await api.get('/msg/detail/' + msgId);
          if (msgRes.status !== 200) {
            let cerr = new Error(msgRes.errMsg);
            cerr.code = msgRes.code;
            throw cerr;
          }
          msgRecord = msgRes.data;

          setread = await api.post('/msg/setreaded', {
            msgId,
            memberId
          });
          if (setread.status !== 200) {
            cerr = new Error(setread.errMsg);
            cerr.code = setread.code;
            throw cerr;
          }
          // console.log('設定已讀回應: ', setread);

          eventConfig = {
            title: msgRecord.title,
            startDate: msgRecord.startUTC,
            endDate: msgRecord.endUTC,
            notes: msgRecord.message
          };
          // console.log('行事曆事件參數: ', eventConfig);
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          await this.setState({
            msgRecord,
            eventConfig,
            isLoading: false,
          });
        }
      }
    );

  }

  protected render() {
    const { navigation } = this.props;

    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      const { eventConfig } = this.state;

      return (
        <Root>
        <Container>
          <Content padder>
            <Card>
              <CardItem header
                onLayout={(event) => {
                  const { width } = event.nativeEvent.layout;
                  carditemWidth = width;
                }}
              >
                <H2>{this.state.msgRecord.title}</H2>
              </CardItem>
              <CardItem bordered>
                <Left>
                  <Text>{this.state.msgRecord.msgType}</Text>
                </Left>
                <Right>
                  <Button small transparent success onPress={()=>{
                      AddCalendarEvent.presentEventCreatingDialog(eventConfig)
                      .then((eventInfo: { calendarItemIdentifier: string, eventIdentifier: string }) => {
                        // handle success - receives an object with `calendarItemIdentifier` and `eventIdentifier` keys, both of type string.
                        // These are two different identifiers on iOS.
                        // On Android, where they are both equal and represent the event id, also strings.
                        // when { action: 'CANCELED' } is returned, the dialog was dismissed
                        console.warn(JSON.stringify(eventInfo));
                      })
                      .catch((error: string) => {
                        // handle error such as when user rejected permissions
                        console.warn(error);
                      });
                  }}>
                    <Text>加到行事曆</Text>
                  </Button>
                </Right>
              </CardItem>
              <CardItem bordered>
                <Body>
                    <Text style={{lineHeight:22}}>{this.state.msgRecord.message}</Text>
                    {this.state.msgRecord.url &&
                      <TouchableOpacity onPress={() => {
                        Linking.openURL(this.state.msgRecord.url).catch((err) => console.error('An error occurred', err));
                      }}>
                      <Text style={{paddingTop:15, color: EStyleSheet.value('$colorMainText'), fontWeight: 'bold',}}>點選前往{this.state.msgRecord.url}</Text>
                      </TouchableOpacity>
                    }
                </Body>
              </CardItem>
              <CardItem footer>
                <Text>{this.state.msgRecord.startFormat}~{this.state.msgRecord.endFormat}</Text>
              </CardItem>
            </Card>

            {
              this.state.msgRecord.attachments &&
              Object.values(this.state.msgRecord.attachments).map(function(attach, key){
                let arratchfile = {};
                if ((/image/).test(attach.content_type)) {
                  const imgRwdWidth = 300;
                  const imgRwdHeight = 225;
                  arratchfile = (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('Msg_Record_Modal',{
                          filesrc: attach.accessurl
                        });
                      }}
                    >
                    <ResponsiveImage source={{ uri: attach.accessurl }} initWidth={imgRwdWidth} initHeight={imgRwdHeight} />
                    </TouchableOpacity>
                  )
                } else {
                  arratchfile = (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('Msg_Record_Modal', {
                          filesrc: attach.accessurl
                        });
                      }}
                    >
                      <Icon type="FontAwesome" size={30} name="files-o" />
                      <Text>{attach.file_name}</Text>
                    </TouchableOpacity>
                  )
                }
                return (
                  <Card key={key}>
                    <CardItem>
                      <Body>
                        {arratchfile}
                      </Body>
                    </CardItem>
                  </Card>
                )
              })
            }

          </Content>

          {<L.IFooter style={styles.colorMsg2} />}
        </Container>
        </Root>
      );
    }
  }
}