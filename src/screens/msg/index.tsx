// 社區公告

import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import {
  Container,
  Content,
  Text,
  Tab,
  Tabs,
  TabHeading,
  List,
  ListItem,
  Body,
  Right,
  Icon,
  Badge
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import { create } from 'apisauce';
import Config from "react-native-config";
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';


class TabList extends Component {

  render() {
    const { navigation, item, memberId } = this.props;
    let textcolor = null;
    if (item.readed) {
      textcolor = { color:'gray'};
    }else{
      textcolor = { color:'black' };
    }

    return (
      <ListItem
        onPress={()=>{
          navigation.navigate('Msg_Record',{
            memberId,
            msgId: item.id,
            headerTitle: item.msgType
          });
        }}
      >
        <Body>
          <Text style={textcolor}>{item.msgType}</Text>
          <Text style={textcolor} note>{item.title}</Text>
        </Body>
        <Right>
          <Text style={textcolor} note>{item.createFormat}</Text>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    )
  }
}
const MsgListItem = withNavigation(TabList);


export default class MsgScreen extends Component<Props> {
  static navigationOptions = ({
    title: '最新消息',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white'
  });

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      mpgs: undefined,
      currentTab: 0
    }
  }

  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        this.setState({
          member: undefined,
          msgs: undefined,
          isLoading: true
        });
      }
    );

    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let msgRes = undefined;
        try {
          member = await ExStorage.getMember(this.props.navigation);
          /**
           * @todo 加入判斷機制導向登入提示！
           */
          // console.log(member);
          const api = create({
            baseURL: Config.API_URL,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          });

          msgRes = await api.get('/msg/list/' + member.unitId + '/' + member.id);
          // console.log('msg get: ',msgRes.data);
          // return ;
          if (msgRes.status !== 200) {
            let cerr = new Error(msgRes.errMsg);
            cerr.code = msgRes.code;
            throw cerr;
          }
          msgs = msgRes.data;
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          await this.setState({
            member,
            msgs,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.msgs);
      }
    );

  }

  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <Content >

            <Tabs initialPage={0} onChangeTab={({ i }) => this.setState({ currentTab: i })} tabBarUnderlineStyle={{ backgroundColor: EStyleSheet.value('$colorSubem') }} >
              <Tab heading={<TabHeading style={styles.tabBackground}>
                <Text style={(this.state.currentTab===0) ? styles.activeTextStyle : styles.textStyle}>社區公告</Text>
                {this.state.msgs.unread && this.state.msgs.unread.news > 0 &&
                  <Badge><Text>{this.state.msgs.unread.news}</Text></Badge>
                }
                </TabHeading>}
                tabStyle={styles.tabBackground}
                activeTabStyle={styles.tabBackground}>
                <List>
                {this.state.msgs.news &&
                  this.state.msgs.news.map((item, key) => {
                    return <MsgListItem key={key} item={item} memberId={this.state.member.id} />
                  })
                }
                </List>
              </Tab>
              <Tab heading={<TabHeading style={styles.tabBackground}>
                <Text style={(this.state.currentTab === 1) ? styles.activeTextStyle : styles.textStyle}>活動通知</Text>
                {this.state.msgs.unread && this.state.msgs.unread.events > 0 &&
                  <Badge><Text>{this.state.msgs.unread.events}</Text></Badge>
                }
                </TabHeading>}
                   tabStyle={styles.tabBackground}
                   activeTabStyle={styles.tabBackground}>
                <List>
                {this.state.msgs.events &&
                  this.state.msgs.events.map((item, key) => {
                    return <MsgListItem key={key} item={item} memberId={this.state.member.id} />
                  })
                }
                </List>
              </Tab>
              <Tab heading={<TabHeading style={styles.tabBackground}>
                <Text style={(this.state.currentTab === 2) ? styles.activeTextStyle : styles.textStyle}>住戶私訊</Text>
                {this.state.msgs.unread && this.state.msgs.unread.units > 0 &&
                  <Badge><Text>{this.state.msgs.unread.units}</Text></Badge>
                }
                </TabHeading>}
                   tabStyle={styles.tabBackground}
                   activeTabStyle={styles.tabBackground}>
                <List>
                {this.state.msgs.units &&
                  this.state.msgs.units.map((item, key) => {
                    return <MsgListItem key={key} item={item} memberId={this.state.member.id} />
                  })
                }
                </List>
              </Tab>
            </Tabs>

          </Content>

          {<L.IFooter />}
        </Container>
      );
    }
  }
}