// 社區公告訊息內容

import React, { Component } from 'react';
import {
  SafeAreaView
} from 'react-native';
import { WebView }from 'react-native-webview';
import {
  Fab,
  Icon,
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import Device from 'react-native-detect-device';


// export default
export default class WebViewModal extends Component<Props> {
  static navigationOptions = {
    headerMode: 'none',
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
    }
  }

  protected render() {
    const { navigation } = this.props;

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          style={{ width: Device.width, height: Device.height, backgroundColor: 'black' }}
          source={{ uri: navigation.getParam('filesrc') }}
          tartInLoadingState={true}
          domStorageEnabled={true}
          javaScriptEnabled={true}
          automaticallyAdjustContentInsets={true}
        />

        <Fab
          style={{ backgroundColor: EStyleSheet.value('$colorSubem') }}
          position="bottomRight"
          onPress={() => this.props.navigation.goBack()}>
          <Icon type="FontAwesome" size={16} name="close" style={{ color: 'white' }} />
        </Fab>
      </SafeAreaView>
    )
  }
}