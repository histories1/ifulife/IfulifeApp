// 我的
import React, { Component } from 'react';
import { SafeAreaView } from 'react-navigation'
import {
  Root,
  Container,
  Content,
  Button,
  Right,
  Body,
  Text,
  H2,
  List,
  ListItem,
  ActionSheet,
  Icon
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import Device from 'react-native-detect-device';
import Settings from "./Settings";
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

let SheetItems = [
  { text: "鼎藏帝景", icon: "american-football", iconColor: "#2c8ef4" },
  { text: "鼎藏璞麗", icon: "analytics", iconColor: "#f42ced" },
  { text: "示範社區", icon: "aperture", iconColor: "#ea943b" },
  { text: "Cancel", icon: "close", iconColor: "#25de5b" }
];
const DESTRUCTIVE_INDEX = 3;
const CANCEL_INDEX = 4;

export default class ProfileSreen extends Component {
  static navigationOptions = ({ navigation }) => {
    let tmp = null;
    if (navigation.getParam('hideGoback') == true ){
      tmp = {headerLeft: null,}
    }
    return {
      title: '我的帳號',
      headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
      headerTintColor: 'white',
      headerRight: ( navigation.getParam('isSignin')==true ) ? (
        // 處理清除登入資訊後回到
        <Button transparent
          onPress={() => {
            ExStorage.removeMember();
            alert('您已完成登出，若要繼續使用請重新登入。');
            navigation.navigate('Home');
          }}
        >
          <Text style={{fontSize:16}}>登出</Text>
          <Icon type="FontAwesome" style={{ color: 'white', }} size={20} name="share-square-o" />
        </Button>
      ) : undefined,
      ...tmp
    }
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined
    }
  }


  componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        let res = await this.setState({
          member: undefined,
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        try {
          member = await ExStorage.getMember();
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          let a = await this.setState({
            member,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
      }
    );
  }


  protected _renderLogin() {
    return (
      <ListItem>
        <Body>
          <H2>用戶, 您好</H2>
          <Text note>若您為社區住戶，請登入以便使用完整功能。</Text>
        </Body>
        <Right>
          <Button small rounded
            onPress={() => {
              this.props.navigation.navigate('Profile_Login');
            }}
          >
            <Text>登入</Text>
          </Button>
        </Right>
      </ListItem>
    )
  }

  protected _renderMemberWithCmts() {
    return (
      <ListItem>
        <Body>
          <H2>{this.state.member.cmt} {this.state.member.unit}住戶 {this.state.member.name},您好</H2>
          <Text note>可使用右側按鈕切換您的社區。</Text>
        </Body>
        <Right>
          <Button small rounded dark
            onPress={() => {
              ActionSheet.show(
                {
                  options: SheetItems,
                  cancelButtonIndex: CANCEL_INDEX,
                  destructiveButtonIndex: DESTRUCTIVE_INDEX,
                  title: "我的社區列表"
                },
                buttonIndex => {
                  this.setState({ clicked: SheetItems[buttonIndex] });
                }
              )
            }
            }
          >
            <Text>切換</Text>
          </Button>
        </Right>
      </ListItem>
    )
  }

  protected _renderMember() {
    return (
      <ListItem>
        <Body>
          <H2>{this.state.member.cmt} {this.state.member.unit}住戶 {this.state.member.name},您好</H2>
          <Text note>使用完畢，您可點選登出確保資料不外洩。</Text>
        </Body>
      </ListItem>
    )
  }


  public render() {

    if (this.state.isLoading ) {
      return (
        <L.PageLoading />
      )
    }else{
      let showInfo = null;
      if (!this.state.member) {
        showInfo = this._renderLogin();
      } else {
        if (this.state.member.hascmts) {
          showInfo = this._renderMemberWithCmts();
        } else {
          showInfo = this._renderMember();
        }
      }

      return (
        <Root>
          <Container>
          <Content>
            <List>
              {showInfo}
            </List>

            <Settings member={this.state.member} />

          </Content>
            {<L.IFooter active="我的帳號" setHeight={(Device.isIos) ? 24 : 48} member={this.state.member} />}
          <SafeAreaView />
          </Container>
        </Root>
      );
    }

  }

}
