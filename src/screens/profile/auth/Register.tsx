// 註冊畫面

import React, { Component } from 'react';
import {
  Container,
  Content,
  Form,
  Item,
  Right,
  Label,
  Input,
  Button,
  H2,
  H3,
  Text,
} from 'native-base';
import { create } from 'apisauce'
import Config from "react-native-config";
import { Buffer } from "buffer";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

const api = create({
  baseURL: Config.API_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
});


export default class RegisterScreen extends Component {
  static navigationOptions = {
    title: '註冊我的裝置',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white',
  }

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      isIdentity: true,
      password: '',
      errmsgPassword: null,
      mappingpassword: '',
      errmsgMappingpassword: null,
      member: this.props.navigation.getParam('member')
    }

    this.postRegister = this.postRegister.bind(this);
  }


  /**
   * 設定密碼完成註冊
   * 取得並設定完整住戶資訊
   */
  async postRegister() {
    try {
      let base64Pwd = new Buffer(this.state.password).toString("base64");
      const reqData = {
        id: this.state.member.id,
        password: base64Pwd
      };
      // console.log('request data: ', reqData);
      await api.post('/authen/register', reqData).then((resp) => {
        // console.log('response data:', resp );
        if( resp.status == 200 ){
          return resp.data;
        }else{
          let cerr = new Error(resp.errMsg);
          cerr.code = resp.code;
          throw cerr;
        }
      }).done( ret => {
        // console.log('result: ', ret);
        const member = { ...this.state.member, ...ret };
        // 使用AsyncStorage儲存
        let final = ExStorage.setMember(member, null);
        this.setState({member});
        // console.log('this.state.member: ', this.state.member);
      });
    } catch (err) {
      console.warn('register post occur unknown error: ',err);
    }
  }

  protected render() {
    const { member } = this.state;

    return (
      <Container>
        <Content padder>
          <Content padder>
            <H3>{member.name}, 您好</H3>
            <Text note>請繼續設定愛富平台App登入密碼即可開始使用完整功能。</Text>
          </Content>
          <Form>
            <Item stackedLabel error={(!this.state.errmsgPassword) ? false : true}>
              <Label>設定登入App密碼</Label>
              <Input onChangeText={(password) => {
                this.setState({ password, errmsgPassword: null });
              }} />
            </Item>
            {this.state.errmsgPassword &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgPassword}</Text>
            }

            <Item stackedLabel error={(!this.state.errmsgMappingpassword) ? false : true}>
              <Label>再一次輸入密碼</Label>
              <Input onChangeText={(mappingpassword) => {
                this.setState({ mappingpassword })
                if (this.state.password !== mappingpassword){
                  this.setState({ errmsgMappingpassword: '密碼不相符，請確認!'});
                }else{
                  this.setState({ errmsgMappingpassword: null});
                }
              }} />
            </Item>
            {this.state.errmsgMappingpassword &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgMappingpassword}</Text>
            }

            <Item style={{ borderBottomWidth: 0, padding: 5 }}>
              <Right>
                <Button block primary
                  // 開通失敗狀況：根據管理後端判斷並紀錄1.不是住戶、2.失效、3.禁止/未啟用
                  onPress={() => {
                    if (!this.state.password) {
                      this.setState({ errmsgPassword: '請設定您的密碼!' });
                      return;
                    }
                    if (this.state.password !== this.state.mappingpassword) {
                      this.setState({ errmsgMappingpassword: '密碼不相符，請確認!' });
                      return ;
                    }
                    this.postRegister();
                    // 註冊成功：引導回我的，同時取得住戶資訊！
                    this.props.navigation.navigate('Profile', {
                      hideGoback: true,
                      isSignin: true,
                      hasCmts: false,
                    });
                  }}>
                  <Text>送出</Text>
                </Button>
              </Right>
            </Item>
          </Form>
        </Content>

        {<L.IFooter />}
      </Container>
    );
  }
}