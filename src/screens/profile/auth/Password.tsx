// 忘記密碼畫面

import React, { Component } from 'react';
import {
  Container,
  Content,
  Form,
  Item,
  Body,
  Right,
  Label,
  Input,
  Button,
  Text,
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import { Buffer } from "buffer";
import ExStorage from 'Ifulife/src/components/ExStorage';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

export default class PasswordScreen extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      isIdentity: false,
      account: '',
      errmsgAccount: null,
      activecode: null,
      errmsgActivecode: null,
      mappingcode: null,
      member: {}
    }

    this.getActivationCode = this.getActivationCode.bind(this);
  }


  /**
   * getActivationCode
   */
  async getActivationCode() {
    const api = create({
      baseURL: Config.API_URL,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    try {
      let passRes = api.get('/authen/identityforreset/' + this.state.account);
      // console.log('password get: ', passRes.data);
      // return ;
      if (passRes.status !== 200) {
        let cerr = new Error(passRes.errMsg);
        cerr.code = passRes.code;
        throw cerr;
      }
      member = passRes.data;
    } catch (err) {
      console.warn('<', err.name, '> identity error: ', err.message);
    } finally {
      await this.setState({
        member,
        mappingcode: res.body.code
      });
    }
  }

  protected render() {
    return (
      <Container>
        <Content padder>
          <Form>

            <Item stackedLabel error={(!this.state.errmsgAccount) ? false : true}>
              <Label>帳號(手機門號)</Label>
              <Input onChangeText={(account) => this.setState({ account })} />

              <Button small bordered info
                onPress={() => {
                  let phone = /^09[0-9]{8}$/
                  if (!this.state.account) {
                    this.setState({ errmsgAccount: '請先輸入手機門號！' })
                    return;
                  } else if (!phone.test(this.state.account)) {
                    this.setState({ errmsgAccount: '手機門號格式錯誤！' })
                    return;
                  } else {
                    this.setState({ errmsgAccount: null });
                  }
                  this.getActivationCode();
                }}
              >
                <Text>取得重設驗證碼</Text>
              </Button>
            </Item>
            {this.state.errmsgAccount &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgAccount}</Text>
            }

            {this.state.mappingcode &&
              (
                <View>
                  <Item stackedLabel error={(!this.state.errmsgActivecode) ? false : true}>
                    <Label>輸入重設驗證碼</Label>
                    <Input onChangeText={(activecode) => this.setState({ activecode })} />
                  </Item>
                  {this.state.errmsgActivecode &&
                    <Text note style={styles.inputErrMsg}>{this.state.errmsgActivecode}</Text>
                  }

                  <Item style={{ borderBottomWidth: 0, padding: 5 }}>
                    <Right>
                      <Button block primary
                        // 開通失敗狀況：根據管理後端判斷並紀錄1.不是住戶、2.失效、3.禁止/未啟用
                        onPress={() => {
                          // console.log('mappingcode: ' +this.state.mappingcode);
                          // console.log(this.state.member);
                          // console.log('activecode: ' +this.state.activecode);
                          if (this.state.mappingcode !== this.state.activecode) {
                            this.setState({ errmsgActivecode: '驗證碼輸入錯誤！' })
                            return;
                          } else {
                            this.setState({ errmsgActivecode: null });
                          }

                          // 開通成功：引導至裝置註冊／設定登入密碼
                          this.props.navigation.navigate('Profile_Register', {
                            member: this.state.member,
                            // 重設密碼流程
                            type: 'reset'
                          });
                        }}>
                        <Text>送出</Text>
                      </Button>
                    </Right>
                  </Item>

                </View>
              )
            }

          </Form>
        </Content>

        {<L.IFooter />}
      </Container>
    );
  }
}