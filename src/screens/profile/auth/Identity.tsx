// 開通畫面

import React, { Component } from 'react';
import { View } from "react-native";
import {
  Container,
  Content,
  Form,
  Item,
  Left,
  Right,
  Label,
  Input,
  Button,
  Text,
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import QRCode from 'react-native-qrcode-svg';
import Device from 'react-native-detect-device';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

export default class IdentityScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: '開通我的帳號',
      headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
      headerTintColor: 'white',
    }
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      account: '',
      errmsgAccount: null,
      member: undefined,
      qrcode: undefined
    }

    this.getActiveQRcode = this.getActiveQRcode.bind(this);
  }


  async getActiveQRcode() {
    const api = create({
      baseURL: Config.API_URL,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    let member = undefined;

    try {
      let idenRes = await api.get('/authen/identity/' + this.state.account);
      // console.log('identify get: ', idenRes.data);
      // return ;
      if (idenRes.status !== 200) {
        let cerr = new Error(idenRes.errMsg);
        cerr.code = idenRes.code;
        throw cerr;
      }
      member = idenRes.data;
    } catch (err) {
      console.warn('<',err.name,'> identity error: ', err.message);
    } finally {
      let a = await this.setState({
        member,
        qrcode: member.code
      });
    }
  }

  protected render() {
    const { width } = Device;
    let qrSize = width - 80;

    return (
      <Container>
        <Content padder>
          <Form>

          <Item stackedLabel error={(!this.state.errmsgAccount) ? false : true}>
            <Label>帳號(手機門號)</Label>
            <Input keyboardType="phone-pad" onChangeText={(account) => this.setState({ account })} placeholder="請先輸入手機門號再點選取得開通碼" />

            <Button small bordered info
              onPress={() => {
                let phone=/^09[0-9]{8}$/
                if(!this.state.account){
                  this.setState({ errmsgAccount: '請先輸入手機門號！'})
                  return ;
                } else if (!phone.test(this.state.account)){
                  this.setState({ errmsgAccount: '手機門號格式錯誤！' })
                  return;
                } else {
                  this.setState({ errmsgAccount: null });
                }
                this.getActiveQRcode();
              }}
            >
              <Text>取得開通碼</Text>
            </Button>
          </Item>
          { this.state.errmsgAccount &&
            <Text note style={styles.inputErrMsg}>{this.state.errmsgAccount}</Text>
          }

          {this.state.qrcode &&
              (
                <View style={{alignItems:'center'}}>
                  <Text />
                  <QRCode size={qrSize} value={this.state.qrcode} />
                  <Text />
                  <Text />
                  <Item style={{ borderBottomWidth: 0, padding: 5 }}>
                    <Right>
                      <Button block primary
                        onPress={() => {
                          // if (this.state.mappingcode !== this.state.activecode){
                          //   this.setState({ errmsgActivecode: '驗證碼輸入錯誤！' })
                          //   return;
                          // }else{
                          //   this.setState({ errmsgActivecode: null });
                          // }

                          // 開通成功：先引導至同意隱私權政策
                          if( this.state.member.is_agree ){
                            // 引導至裝置註冊／設定登入密碼
                            this.props.navigation.navigate('Profile_Register', {
                              member: this.state.member
                            });
                          } else {
                            this.props.navigation.navigate('AgreePage', {
                              member: this.state.member
                            });
                          }
                        }}>
                        <Text>模擬開通成功</Text>
                      </Button>
                    </Right>
                  </Item>

                </View>
              )
            }

          </Form>
        </Content>

        {<L.IFooter />}
      </Container>
    );
  }
}