// 登入畫面

import React, { Component } from 'react';
import { Alert } from "react-native";
import {
  Container,
  Content,
  Form,
  Item,
  Body,
  Right,
  Label,
  Input,
  Button,
  Text,
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import { Buffer } from "buffer";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

export default class LoginScreen extends Component {
  static navigationOptions = {
    title: '登入',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white',
  }

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      isIdentity: true,
      password: '',
      mappingpassword: '',
      errmsgMappingpassword: null,
      member: this.props.navigation.getParam('member')
    }

    this.postLogin = this.postLogin.bind(this);
  }

  /**
   * 設定密碼完成註冊
   * 取得並設定完整住戶資訊
   */
  async postLogin() {
    const api = create({
      baseURL: Config.API_URL,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    try {
      let base64Pwd = new Buffer(this.state.password).toString("base64");
      let loginRes = await api.post('/authen/login', {
        account: this.state.account,
        password: base64Pwd
      });
      // console.log('login post: ', loginRes);
      // return ;
      if (loginRes.status !== 200) {
        let cerr = new Error(loginRes.errMsg);
        cerr.code = loginRes.code;
        throw cerr;
      }
      const respJson = loginRes.data;
      let a = null;
      a = await this.setState({
        member: respJson
      });
      // 將回傳內容使用AsyncStorage處理
      await ExStorage.setMember(respJson, null);
      // console.log(this.state.member);
      // 登入成功回到我的畫面
      this.props.navigation.navigate('Profile', {
        hideGoback: true,
        isSignin: true,
        hasCmts: false
      });
    } catch (err) {
      // if (err.message == "loginnRes is not defined"){
      // }else{
      //   console.log('message: ', err.message);
      // }
      alert('登入資訊錯誤，請重新操作');
      this.setState({
        account: '',
        password: ''
      });
    }
  }


  protected render() {
    return (
      <Container>
        <Content padder>
          <Form>
            <Item stackedLabel error={(!this.state.errmsgAccount) ? false : true}>
              <Label>帳號(手機門號)</Label>
              <Input
                keyboardType="phone-pad"
                value={this.state.account}
                onChangeText={(account) => {
                  this.setState({ account, errmsgAccount: null });
                }}
              />
            </Item>
            {this.state.errmsgAccount &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgAccount}</Text>
            }

            <Item stackedLabel error={(!this.state.errmsgPassword) ? false : true}>
              <Label>密碼</Label>
              <Input
                value={this.state.password}
                onChangeText={(password) => {
                  this.setState({ password, errmsgPassword: null })
                }}
              />
            </Item>
            {this.state.errmsgPassword &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgPassword}</Text>
            }

            <Item style={{ borderBottomWidth: 0, padding: 5 }}>
              <Body>
                <Button bordered
                  onPress={() => {
                    this.props.navigation.navigate('Profile_Identity');
                  }}
                >
                  <Text>我要開通</Text>
                </Button>
              </Body>
              <Right>
                <Button block primary
                  onPress={ () => {
                    if (!this.state.account) {
                      this.setState({ errmsgAccount: '請輸入您的帳號!' });
                      return;
                    } else if (!this.state.password) {
                      this.setState({ errmsgPassword: '請輸入您的密碼!' });
                      return;
                    }

                    this.postLogin();
                  }}>
                  <Text>送出</Text>
                </Button>
              </Right>
            </Item>

          </Form>

          <Text note style={{marginTop:5}}>首次使用程式住戶，請直接點選我要開通，不需輸入帳號密碼。</Text>

        </Content>

        {<L.IFooter />}
      </Container>
    );
  }
}