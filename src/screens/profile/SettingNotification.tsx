import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Left,
  Right,
  Text,
  List,
  ListItem,
  Switch,
  Icon
} from 'native-base';

export default class SettingNotificationScreen extends Component {
  constructor(props) {
    super(props);


    this.state = {
      member: this.props.navigation.getParam('member'),
      ntype_system: false,
      ntype_msg: false,
      ntype_mpg_receive: false,
      ntype_mpg_take: false,
      ntype_mpg_reject: false,
    }
  }



  protected render() {
    const { member } = this.state;

    return (
      <View>
        <List>
          <ListItem itemDivider>
            <Text>接收推播訊息項目</Text>
          </ListItem>
          <ListItem noIndent>
            <Left>
              <Text>開啟管理系統訊息通知</Text>
            </Left>
            <Right>
              <Switch
                trackColor={'green'}
                value={this.state.ntype_system}
                onChange={() => {
                  this.setState({ ntype_system: !this.state.ntype_system });
                }}
              />
            </Right>
          </ListItem>

          <ListItem noIndent>
            <Left>
              <Text>開啟最新消息通知</Text>
            </Left>
            <Right>
              <Switch
                trackColor={'green'}
                value={this.state.ntype_msg}
                onChange={() => {
                  this.setState({ ntype_msg: !this.state.ntype_msg });
                }}
              />
            </Right>
          </ListItem>

          <ListItem noIndent>
            <Left>
              <Text>開啟包裹到了通知</Text>
            </Left>
            <Right>
              <Switch
                trackColor={'green'}
                value={this.state.ntype_mpg_receive}
                onChange={() => {
                  this.setState({ ntype_mpg_receive: !this.state.ntype_mpg_receive });
                }}
              />
            </Right>
          </ListItem>

          <ListItem noIndent>
            <Left>
              <Text>開啟包裹已領件通知</Text>
            </Left>
            <Right>
              <Switch
                trackColor={'green'}
                value={this.state.ntype_mpg_take}
                onChange={() => {
                  this.setState({ ntype_mpg_take: !this.state.ntype_mpg_take });
                }}
              />
            </Right>
          </ListItem>

          <ListItem noIndent>
            <Left>
              <Text>開啟包裹已退件通知</Text>
            </Left>
            <Right>
              <Switch
                trackColor={'green'}
                value={this.state.ntype_mpg_reject}
                onChange={() => {
                  this.setState({ ntype_mpg_reject: !this.state.ntype_mpg_reject });
                }}
              />
            </Right>
          </ListItem>
        </List>

      </View>
    )
  }
}