import React, { Component } from 'react';
import { View, Alert } from 'react-native';
import { withNavigation } from 'react-navigation'
import {
  Right,
  Body,
  Text,
  List,
  ListItem,
  Accordion,
  Switch,
  Card,
  CardItem,
  Icon
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import ExStorage from 'Ifulife/src/components/ExStorage';

class SettingScreen extends Component {
  constructor(props) {
    super(props);

    if (!this.props.member){
      act = false;
    } else if (this.props.member.is_private == 1){
      act = true;
    }else{
      act = false;
    }

    this.state = {
      privatetakeEnable: act ,
      //notice : 加入推播機制後替換成動態設定！
      noticeEnable: true,
    }

    this.onChangePrivatetake = this.onChangePrivatetake.bind(this);
  }

  private _renderAccordionHeader(item, expanded) {
    return (
      <View style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "#A9DAD6"
      }}>
        <Text style={{ fontWeight: "600" }}>
          {" "}{item.title}
        </Text>
        {expanded
          ? <Icon type="FontAwesome" size={18} name="chevron-down" />
          : <Icon type="FontAwesome" size={18} name="chevron-left" />}
      </View>
    );
  }

  async setPrivate(isPrivate) {
    const api = create({
      baseURL: Config.API_URL,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    let setResult = undefined;
    const { member } = this.props;
    try{
      setRes = await api.post('/authen/setPrivate', {
        memberId: member.id,
        isPrivate: (!isPrivate) ? 0 : 1
      });
      if (setRes.status != 200 ){
        const cerr = new Error(setRes.data.customMsg);
        cerr.code = setRes.data.code;
        throw cerr;
      }
      setResult = setRes.data;
      member["is_private"] = (!isPrivate) ? 0 : 1;
      await ExStorage.setMember(member, null);
      alert(setResult.msg);
      return true;
    } catch( err ) {
      console.warn('發生錯誤: ', err.message);
      alert('發生不明錯誤無法設定私人領件！');
      return false;
    }

  }

  async onChangePrivatetake() {
    const isPrivate = !this.state.privatetakeEnable;
    console.log('目前配置:', isPrivate );
    res = await this.setPrivate(isPrivate);
    if( res ){
      this.setState({
        privatetakeEnable: isPrivate
      });
    }
    // console.log('privatetakeEnable:', this.state.privatetakeEnable);
  }

  protected render() {
    const { member, navigation } = this.props;

    console.log('載入變數:', global.config);

    const AccordionArray = [
      { title: "住戶如何開通呢？", content: "住戶開通前須交住戶資料表，請洽貴社區管理中心，相關人員將會為您開通喔！" },
      { title: "我更換新手機後是否需要重新註冊呢？", content: "若您先前已有註冊過並登入使用，請下載『愛富生活』後以原註冊帳號登入即可，若忘記帳號，可至管理中心請相關人員為您查詢。" }
    ];

    return (
      <View>
      <List>
        <ListItem itemDivider>
          <Text>個人設定</Text>
        </ListItem>
        <ListItem onPress={() => {
            if (!member) {
              alert('您必須登入後才能設定此功能!');
              return;
            }
            navigation.navigate('Profile_EditPassword', { member });
          }}
        >
          <Body>
            <Text>修改我的密碼</Text>
          </Body>
          <Right>
            <Icon type="FontAwesome"  name="chevron-right" />
          </Right>
        </ListItem>

        <ListItem itemDivider>
          <Text>包裹領件設定</Text>
        </ListItem>
        <ListItem>
          <Body>
            <Text>僅供本人親自領取</Text>
          </Body>
          <Right>
            <Switch
              trackColor={'green'}
              value={this.state.privatetakeEnable}
              onChange={() => {
                if( !member ){
                  alert('您必須登入後才能設定此功能!');
                  return ;
                }
                this.onChangePrivatetake();
              }}
            />
          </Right>
        </ListItem>

        <ListItem itemDivider>
          <Text>設定推播通知</Text>
        </ListItem>
        <ListItem>
          <Body>
            <Text>開啟接收推播通知</Text>
          </Body>
          <Right>
            <Switch
              trackColor={'green'}
              value={this.state.noticeEnable}
              onChange={()=>{
                this.setState({ noticeEnable: !this.state.noticeEnable });
              }}
              disabled
            />
          </Right>
        </ListItem>
        <ListItem onPress={() => {
          if( !member ){
            alert('您必須登入後才能設定此功能!');
            return ;
          }
          navigation.navigate('Setting_Notification', {
            member
          })
        }}>
          <Body>
            <Text>推播通知項目</Text>
          </Body>
          <Right>
            <Icon type="FontAwesome" name="chevron-right" />
          </Right>
        </ListItem>

        <ListItem itemDivider>
          <Text>愛富生活服務政策</Text>
        </ListItem>
        <ListItem onPress={() => {
          navigation.navigate('Statement', {
            frompage: 'setting'
          });
        }}>
          <Body>
            <Text>會員服務條款</Text>
          </Body>
          <Right>
            <Icon type="FontAwesome" name="chevron-right" />
          </Right>
        </ListItem>
        <ListItem onPress={() => {
          navigation.navigate('Privacy', {
            frompage: 'setting'
          });
        }}>
          <Body>
            <Text>隱私權保護政策</Text>
          </Body>
          <Right>
            <Icon type="FontAwesome" name="chevron-right" />
          </Right>
        </ListItem>
      </List>

      <Card>
        <CardItem header>
          <Text>常見問題</Text>
        </CardItem>
        <CardItem>
          <Accordion
            dataArray={AccordionArray}
            expanded={0}
            renderHeader={this._renderAccordionHeader}
          />
        </CardItem>
      </Card>
      </View>
    )
  }
}
export default withNavigation(SettingScreen);