// 修改我的密碼

import React, { Component } from 'react';
import {
  Container,
  Content,
  Form,
  Item,
  Right,
  Label,
  Input,
  Button,
  H3,
  Text,
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import { Buffer } from "buffer";
import EStyleSheet from 'react-native-extended-stylesheet';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

export default class EditPasswordScreen extends Component {
  static navigationOptions = {
    title: '修改登入密碼',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white',
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      oldpassword: undefined,
      errmsgOldPassword: undefined,
      newpassword: undefined,
      errmsgNewpassword: undefined,
      mappingpassword: undefined,
      errmsgMappingpassword: undefined,
      member: this.props.navigation.getParam('member')
    }

    this.postEditPassword = this.postEditPassword.bind(this);
  }


  /**
   * 設定密碼完成註冊
   * 取得並設定完整住戶資訊
   */
  async postEditPassword() {
    const api = create({
      baseURL: Config.API_URL,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    const base64OldPwd = new Buffer(this.state.oldpassword).toString("base64");
    const base64NewPwd = new Buffer(this.state.newpassword).toString("base64");
    let result = undefined;
    try {
      const editRes = await api.post('/authen/editpassword', {
        memberId: this.state.member.id,
        oldpassword: base64OldPwd,
        newpassword: base64NewPwd
      });
      // console.log('editpassword post: ', editRes.data);
      // return ;
      if (editRes.status !== 200) {
        // console.log(editRes);
        const cerr = new Error(editRes.data.customMsg);
        cerr.code = editRes.data.code;
        throw cerr;
      }
      result = editRes.data;
    } catch (err) {
      // if( err.code == 4013 ){
        result = {
          codeType: 'FAIL',
          msg: err.message,
        }
      // }
      // console.warn('<', err.name, '> identity error: ', err.message);
    } finally {
      alert(result.msg);
      if(result.codeType == 'OK'){
        // 修改成功：引導回我的，同時顯示alter提示！
        this.props.navigation.navigate('Profile');
      }else{
        this.setState({
          oldpassword: undefined,
          errmsgOldPassword: undefined,
          newpassword: undefined,
          errmsgNewpassword: undefined,
          mappingpassword: undefined,
          errmsgMappingpassword: undefined,
        });
      }
    }
  }

  protected render() {
    const { member } = this.state;
    // console.log( 'editpassword member: ', member );

    return (
      <Container>
        <Content padder>
          <Form>
            <Item stackedLabel error={(!this.state.errmsgOldPassword) ? false : true}>
              <Label>輸入舊的登入密碼</Label>
              <Input value={this.state.oldpassword} onChangeText={(oldpassword) => {
                this.setState({ oldpassword, errmsgOldPassword: null });
              }} />
            </Item>
            {this.state.errmsgOldPassword &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgOldPassword}</Text>
            }

            <Item stackedLabel error={(!this.state.errmsgPassword) ? false : true}>
              <Label>輸入新的登入密碼</Label>
              <Input value={this.state.newpassword} onChangeText={(newpassword) => {
                this.setState({ newpassword, errmsgNewpassword: null, errmsgMappingpassword: null });
              }} />
            </Item>
            {this.state.errmsgNewpassword &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgNewpassword}</Text>
            }

            <Item stackedLabel error={(!this.state.errmsgMappingpassword) ? false : true}>
              <Label>再一次輸入新密碼</Label>
              <Input value={this.state.mappingpassword} onChangeText={(mappingpassword) => {
                this.setState({ mappingpassword })
                if (this.state.newpassword !== mappingpassword) {
                  this.setState({ errmsgMappingpassword: '新密碼不相符，請確認!' });
                } else {
                  this.setState({ errmsgMappingpassword: null });
                }
              }} />
            </Item>
            {this.state.errmsgMappingpassword &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgMappingpassword}</Text>
            }

            <Item style={{ borderBottomWidth: 0, padding: 5 }}>
              <Right>
                <Button block primary
                  onPress={() => {
                    if (!this.state.oldpassword) {
                      this.setState({ errmsgOldPassword: '請輸入原本登入密碼!' });
                      return;
                    } else if (!this.state.newpassword) {
                      this.setState({ errmsgNewpassword: '請輸入新的登入密碼!' });
                      return;
                    } else if (!this.state.mappingpassword) {
                      this.setState({ errmsgMappingpassword: '請再次輸入新的登入密碼!' });
                      return;
                    } else if (this.state.newpassword !== this.state.mappingpassword) {
                      this.setState({ errmsgMappingpassword: '新密碼不相符，請確認!' });
                      return;
                    }
                    this.postEditPassword();
                  }}>
                  <Text>送出</Text>
                </Button>
              </Right>
            </Item>
          </Form>
        </Content>

        {<L.IFooter />}
      </Container>
    );
  }
}