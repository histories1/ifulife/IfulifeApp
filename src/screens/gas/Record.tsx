// 意見提出紀錄

import React, { Component } from 'react';
import {
  Container,
  Content,
} from 'native-base';
import moment from 'moment';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import NonRecord from "./NonRecord";
import Recorded from "./Recorded";
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

const api = create({
  baseURL: Config.API_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
});

export default class GasRecordScreen extends Component {
  static navigationOptions = {
    title: '抄錶紀錄',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white'
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      yearmonthArray: undefined,
      gasRecords: undefined,
      member: undefined,
    }
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  async componentWillMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        let res = await this.setState({
          member: undefined,
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let yearmonthArray = undefined;
        let gasRecords = undefined;
        try {
          member = await ExStorage.getMember(this.props.navigation);
          yearmonthArray = await this._setYearMonth();
          gasRecords = await this._getRecords(member.unitId);
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          await this.setStateAsync({
            member,
            yearmonthArray,
            gasRecords,
            isLoading: false,
          });
        }
      }
    );
  }


  /**
   * @todo : 需要參數！
   * 1. 開始抄表月份
   * 2. 隔多久超一次
   * 3. 每個紀錄月份截止抄表日期
   */
  async _setYearMonth() {
    let yearmonthArray = [];
    const now = new Date();
    // 時間區間
    let sectionCount = moment([now.getFullYear(), now.getMonth(), 1]).diff(moment([2018, 1, 1]), 'months', true);
    const section = 2;
    // console.log('時間區間: ',section);
    yearmonthArray.push(moment(now).format('YYYY-MM'));
    let limit = Number.parseInt(sectionCount / section, 10);
    // console.log('追溯幾個區間: ', limit);
    let setMoment = moment(now);
    for (let i = 0; i < limit; i++) {
      let key = setMoment.subtract(section, 'month').format('YYYY-MM');
      // console.log(key);
      yearmonthArray.push(key);
      // yearmonthArray[`${key}`]={};
    }
    return yearmonthArray;
  }


  async _getRecords(unitId) {
    try {
      let recordsRes = await api.get('/gas/list/' + unitId);
      // console.log('gas get records: ', recordsRes.data);
      if (recordsRes.status !== 200) {
        let cerr = new Error(recordsRes.errMsg);
        cerr.code = recordsRes.code;
        throw cerr;
      }
      return recordsRes.data;
    } catch (err) {
      console.warn('reject others: ', err);
    }
  }

  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <Content>
            {this.state.yearmonthArray &&
              this.state.yearmonthArray.map((year_month, i) => {
                if( this.state.gasRecords[year_month] ){
                  // console.log('找到： ',this.state.gasRecords[year_month]);
                  return <Recorded key={i} record={this.state.gasRecords[year_month]} />
                }else{
                  return <NonRecord key={i} year_month={year_month} />
                }
              })
            }
          </Content>
          {<L.IFooter style={styles.colorGas2} />}
        </Container>
      );
    }
  }
}