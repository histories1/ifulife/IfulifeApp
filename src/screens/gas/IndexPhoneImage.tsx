import React, { Component } from 'react';
import {
  Content,
} from 'native-base';
import ResponsiveImage from 'react-native-responsive-image';
import Device from 'react-native-detect-device';

export default class PhoneImage extends Component {
  render() {
    const { width } = Device;

    imgWidth = width - 10;
    imgHeigt = imgWidth * 0.75;

    return (
      <Content contentContainerStyle={{ alignItems: 'center' }}>
        <ResponsiveImage
          initWidth={imgWidth}
          initHeight={imgHeigt}
          source={require('Ifulife/src/assets/gas/gas1.png')}
          resizeMode="contain"
        />
        <ResponsiveImage
          initWidth={imgWidth}
          initHeight={imgHeigt}
          source={require('Ifulife/src/assets/gas/gas2.png')}
          resizeMode="contain"
        />
        <ResponsiveImage
          initWidth={imgWidth}
          initHeight={imgHeigt}
          source={require('Ifulife/src/assets/gas/gas3.png')}
          resizeMode="contain"
        />
        <ResponsiveImage
          initWidth={imgWidth}
          initHeight={imgHeigt}
          source={require('Ifulife/src/assets/gas/gas4.png')}
          resizeMode="contain"
        />
      </Content>
    )
  }
}