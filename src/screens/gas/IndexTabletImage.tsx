import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-native-easy-grid';
import ResponsiveImage from 'react-native-responsive-image';

export default class TabletImage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      docimgBlockWidth: null,
      docimgBlockHeight: null,
    }
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col style={{ justifyContent: 'center', alignItems: 'center', }}
            onLayout={(event) => {
              var { width, height } = event.nativeEvent.layout;
              this.setState({
                docimgBlockWidth: width,
                docimgBlockHeight: height,
              });

            }}>
            <ResponsiveImage
              initWidth={this.state.docimgBlockWidth - 80}
              initHeight={this.state.docimgBlockHeight - 20}
              source={require('Ifulife/src/assets/gas/gas1.png')}
              resizeMode="contain"
            />
          </Col>
          <Col style={{ justifyContent: 'center', alignItems: 'center', }}>
            <ResponsiveImage
              initWidth={this.state.docimgBlockWidth - 80}
              initHeight={this.state.docimgBlockHeight - 20}
              source={require('Ifulife/src/assets/gas/gas2.png')}
              resizeMode="contain"
            />
          </Col>
        </Row>
        <Row>
          <Col style={{ justifyContent: 'center', alignItems: 'center', }}>
            <ResponsiveImage
              initWidth={this.state.docimgBlockWidth - 80}
              initHeight={this.state.docimgBlockHeight - 20}
              source={require('Ifulife/src/assets/gas/gas3.png')}
              resizeMode="contain"
            />
          </Col>
          <Col style={{ justifyContent: 'center', alignItems: 'center', }}>
            <ResponsiveImage
              initWidth={this.state.docimgBlockWidth - 80}
              initHeight={this.state.docimgBlockHeight - 20}
              source={require('Ifulife/src/assets/gas/gas4.png')}
              resizeMode="contain"
            />
          </Col>
        </Row>
      </Grid>
    )
  }
}