import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import {
  Body,
  Right,
  Text,
  Card,
  CardItem,
  Button
} from 'native-base';

class NonRecordCom extends Component {
  render() {
    const { year_month, navigation } = this.props;
    // console.log('porps: ', this.props);
    return (
      <Card>
        <CardItem header>
          <Body>
            <Button small block
              onPress={() => {
                navigation.navigate('Gas', {
                  year_month
                });
              }}
            >
              <Text>點此建立紀錄</Text>
            </Button>
            <Text note>未紀錄度數</Text>
          </Body>
          <Right>
            <Text>{year_month}</Text>
          </Right>
        </CardItem>
      </Card>
    )
  }
}
const NonRecord = withNavigation(NonRecordCom);
export default NonRecord;