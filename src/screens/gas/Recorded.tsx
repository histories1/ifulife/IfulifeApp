import React, { Component } from 'react';
import {
  Body,
  Right,
  H3,
  Text,
  Card,
  CardItem,
} from 'native-base';

export default class Recorded extends Component {
  render() {
    const { record } = this.props;
    return (
      <Card>
        <CardItem header>
          <Body>
            <H3>{record.degree}度</H3>
            <Text note>{record.memberName}紀錄於{record.updated_at}</Text>
          </Body>
          <Right>
            <Text>{record.year_month.replace('-01', '')}</Text>
          </Right>
        </CardItem>
      </Card>
    )
  }
}