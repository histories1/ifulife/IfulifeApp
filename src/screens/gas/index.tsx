// 瓦斯抄錶
import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  Alert,
  Keyboard
} from 'react-native';
import {
  Button,
  Container,
  Content,
  Body,
  H1,
  H2,
  H3,
  Item,
  Text,
} from 'native-base';
import moment from 'moment';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import { Col, Grid, Row  } from 'react-native-easy-grid';
import { TextInputMask } from 'react-native-masked-text'
import Device from 'react-native-detect-device';

import TabletImage from "./IndexTabletImage";
import PhoneImage from './IndexPhoneImage';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';



// tslint:disable-next-line: max-classes-per-file
export default class GasSecreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: '瓦斯抄錶',
      headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
      headerTintColor: 'white',
      headerRight: (
        <TouchableOpacity
          style={{ marginRight:10 }}
          onPress={()=>{
            navigation.navigate('Gas_Record');
          }}
        >
          <Text style={{ color: 'white', fontSize: 20}}>紀錄</Text>
        </TouchableOpacity>
      )
    }
  };

  constructor(props) {
    super(props)

    this.state = {
      docimgBlockWidth: undefined,
      docimgBlockHeight: undefined,
      // 顯示用
      displayYearMonth: undefined,
      deadlineDate: undefined,
      // formData
      setyear: undefined,
      setmonth: undefined,
      degree: undefined,
      showFooter: true
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  _keyboardDidShow() {
    this.setState({ showFooter: false });
  }
  _keyboardDidHide() {
    this.setState({ showFooter: true });
  }

  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        //Current Date
        const now = new Date();
        // Moment.locale('zh-tw');
        const setyear = moment(now).format('YYYY');
        const setmonth = moment(now).format('MM');

        let res = await this.setState({
          member: undefined,
          year_month: `${setyear}-${setmonth}`,
          displayYearMonth: `${setyear}年${setmonth}月`,
          deadlineDate: moment(now).endOf('month').format('YYYY-MM-DD'),
          degree: undefined,
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let year_month = this.props.navigation.getParam('year_month');
        if( year_month ){
          let setval = year_month.split('-');
          const setyear = setval[0];
          const setmonth = setval[1];
          this.setState({
            year_month: `${setyear}-${setmonth}`,
            displayYearMonth: `${setyear}年${setmonth}月`,
          });
        }

        let member = undefined;
        try {
          member = await ExStorage.getMember(this.props.navigation);
          // 從紀錄[紙本]點選時傳送要設定的年月
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          let a = await this.setState({
            member,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
      }
    );
  }


  async formPost() {
    let formData = new FormData();
    formData.append('memberId', this.state.member.id);
    formData.append('year_month', this.state.year_month);
    formData.append('degree', this.state.degree);
    // console.log('formData: ',formData);

    try {
      // 呼叫apisauce
      const api = create({
        baseURL: Config.API_URL,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
        }
      });
      api.post('/gas/form', formData)
        .then((resp) => {
          if (resp.status == 200) {
            // console.log('response data:', resp.data );
            Alert.alert(
              '本次' + resp.data.date + '瓦斯紀錄建檔完成',
              resp.data.msg,
              [
                { text: '前往', onPress: () => { this.props.navigation.navigate('Gas_Record'); } },
              ],
              { cancelable: false },
            );
          } else {
            let cerr = new Error(resp.errMsg);
            cerr.code = resp.code;
            throw cerr;
          }
        }).done(ret => {
          console.log('result: ', ret);
          // 提示成功 => 引導到提出紀錄畫面
        });
    } catch (err) {
      console.warn('gas posted error: ', err);
    }

  }


  protected render() {

    let ImageBlock = null;

    if (Device.isAndroid && !this.state.showFooter) {
      ImageBlock =<View />
    } else if (Device.isTablet ){
      ImageBlock = <Row size={6}><TabletImage /></Row>
    } else {
      ImageBlock = <Row size={5}><PhoneImage /></Row>
    }

    return (
      <Container>
        <Grid>
          <Row size={Device.isTablet ? 5 : 4}>
            <Grid>
              <Row size={3}
                style={{ backgroundColor: EStyleSheet.value('$textColor')}}>
                {(Device.isTablet) ?
                  (
                <Content padder
                  style={{
                    marginHorizontal:20,
                    marginVertical: 10,
                    borderRadius:20,
                    backgroundColor:'white'
                  }}>
                  <H2>請填寫{this.state.displayYearMonth}瓦斯登記表</H2>
                  <Item regular>
                    <TextInputMask
                      style={styles.gasInput}
                      type={'custom'}
                      keyboardType={'numeric'}
                      options={
                        {
                          mask: '99999'
                        }
                      }
                      // dont forget to set the "value" and "onChangeText" props
                      value={this.state.degree}
                      onChangeText={text => {
                        this.setState({
                          degree: text
                        })
                      }}
                    />
                  </Item>
                  <H2 style={{ alignItems: 'flex-end', }}>本期應於{this.state.deadlineDate}之前填寫完成</H2>
                  <View style={{width:"100%", height:100, justifyContent:"center", alignItems: 'center',}}>
                    <Button bordered style={{ borderColor: 'white', }}
                      onPress={() => {
                        this.formPost();
                      }}>
                      <Text style={{ color: 'white' }}>儲存</Text>
                    </Button>
                  </View>
                </Content>
                  ) :
                  (
                    <View style={{
                        width: Device.width-20,
                        height: 160,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginHorizontal: 10,
                        marginVertical: 5,
                        borderRadius: 10,
                        backgroundColor: 'white'
                      }}>
                      <Text style={{marginVertical: 10, fontSize:18}}>請填寫{this.state.displayYearMonth}瓦斯登記表</Text>
                      <Item rounded style={{width:'80%'}}>
                        <TextInputMask
                          style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            marginHorizontal: '5%',
                            width: '90%',
                            height: 40
                          }}
                          keyboardType={'numeric'}
                          type={'custom'}
                          options={
                            {
                              mask: '99999'
                            }
                          }
                          // dont forget to set the "value" and "onChangeText" props
                          value={this.state.degree}
                          onChangeText={text => {
                            this.setState({
                              degree: text
                            })
                          }}
                        />
                      </Item>
                      <Text style={{marginVertical: 5,}}>本期應於{this.state.deadlineDate}之前填寫完成</Text>
                      <View style={styles.center}>
                        <Button bordered small
                          onPress={() => {
                            this.formPost();
                          }}>
                          <Text>儲存</Text>
                        </Button>
                      </View>
                    </View>
                  )
                }
              </Row>
              <Row size={2}
                style={{
                  backgroundColor: EStyleSheet.value('$textColor')
                }}>

                { ( Device.isTablet ) ?
                  (
                    <Content>
                      <H1 style={{
                        color: 'white'
                      }}>瓦斯登記範例</H1>
                      <H1 style={{
                        color: 'white'
                      }}>請分別依照您府上的表型</H1>
                      <H1 style={{
                        color: 'white'
                      }}>由左至右(包括0)玻璃框內的數字，填寫表內</H1>
                    </Content>
                  )
                    :
                  (
                  <View style={{paddingHorizontal:10, paddingTop: 20}}>
                    <Text style={{ color: 'white', fontSize: 20, lineHeight:24 }}>瓦斯登記範例：請分別依照您府上的表型，由左至右(包括0)玻璃框內的數字，填寫表內</Text>
                  </View>
                  )
                }
              </Row>
            </Grid>
          </Row>
          {ImageBlock}
        </Grid>

        {<L.IFooter member={this.state.member} setHeight={(Device.isIos) ? 24 : 48} />}
      </Container>
    );
  }
}