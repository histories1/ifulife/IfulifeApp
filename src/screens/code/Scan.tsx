import React, { Component } from 'react';
import { SafeAreaView } from 'react-navigation'
import { StyleSheet, View, Text, Platform, TouchableOpacity, Linking, PermissionsAndroid } from 'react-native';
import {
  Container,
  Content
} from 'native-base';
import Device from 'react-native-detect-device';
import ExStorage from 'Ifulife/src/components/ExStorage';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import L from 'Ifulife/src/components/Layout';

export default class CodeScanScreen extends Component {
  static navigationOptions = {
    title: '拍照掃碼'
  }

  constructor(porps) {
    super(porps);

    this.state = {
      isLoading: true,
      member: undefined,
      QR_Code_Value: '',
      Start_Scanner: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener(
      'willFocus', async (payload) => {
        let member = undefined;
        try {
          member = await ExStorage.getMember(navigation);
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          this.setState({
            member: member,
            isLoading: false
          });
        }
        // console.log('will focus', member);
      }
    );
    navigation.addListener(
      'willBlur', async (payload) => {
        this.setState({
          member: undefined,
          isLoading: false
        });
        // console.log('willBlur', payload);
      }
    );
  }

  openLink_in_browser = () => {
    Linking.openURL(this.state.QR_Code_Value);
  }

  onQR_Code_Scan_Done = (QR_Code) => {
    this.setState({ QR_Code_Value: QR_Code });
    this.setState({ Start_Scanner: false });
  }

  open_QR_Code_Scanner = () => {
    let that = this;

    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA, {
              'title': 'Camera App Permission',
              'message': 'Camera App needs access to your camera '
            }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {

            that.setState({ QR_Code_Value: '' });
            that.setState({ Start_Scanner: true });
          } else {
            alert("CAMERA permission denied");
          }
        } catch (err) {
          alert("Camera permission err", err);
          console.warn(err);
        }
      }
      requestCameraPermission();
    } else {
      that.setState({ QR_Code_Value: '' });
      that.setState({ Start_Scanner: true });
    }
  }


  render() {
    if (!this.state.Start_Scanner) {

      return (
        <Container>
          <Content>
            <Text style={{ fontSize: 22, textAlign: 'center' }}>React Native Scan QR Code Example</Text>

            <Text style={styles.QR_text}>
              {this.state.QR_Code_Value ? 'Scanned QR Code: ' + this.state.QR_Code_Value : ''}
            </Text>

            {this.state.QR_Code_Value.includes("http") ?
              <TouchableOpacity
                onPress={this.openLink_in_browser}
                style={styles.button}>
                <Text style={{ color: '#FFF', fontSize: 14 }}>Open Link in default Browser</Text>
              </TouchableOpacity> : null
            }

            <TouchableOpacity
              onPress={this.open_QR_Code_Scanner}
              style={styles.button}>
              <Text style={{ color: '#FFF', fontSize: 14 }}>
                Open QR Scanner
              </Text>
            </TouchableOpacity>
          </Content>

          {<L.IFooter active="掃描" member={this.state.member} setHeight={(Device.isIos) ? 24 : 48} />}
          <SafeAreaView />
        </Container>
      );
    }

    return (
      <Container>
        <CameraKitCameraScreen
          showFrame={true}
          scanBarcode={true}
          laserColor={'#FF3D00'}
          frameColor={'#00C853'}
          colorForScannerFrame={'black'}
          onReadCode={event =>
            this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
          }
        />
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  QR_text: {
    color: '#000',
    fontSize: 19,
    padding: 8,
    marginTop: 12
  },
  button: {
    backgroundColor: '#2979FF',
    alignItems: 'center',
    padding: 12,
    width: 300,
    marginTop: 14
  },
});