import React, { Component } from 'react';
import {
  Container,
  Content,
  Body,
  H2,
  H3,
  Text
} from 'native-base';
import Barcode from 'react-native-barcode-builder';
import QRCode from 'react-native-qrcode-svg';
import Device from 'react-native-detect-device';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

export default class CodeShowScreen extends Component {
  static navigationOptions = {
    title: '我的專屬條碼'
  }

  constructor(porps) {
    super(porps);

    const member = this.props.navigation.getParam('member');
    // console.log('member: ', this.props.navigation.getParam('member'));

    this.state = {
      Code_Value: member.activation_code,
      isLoading: false,
    };
  }


  render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      const { width } = Device;
      let qrSize = width-80;

      return (
        <Container>
          <Content padder>
            <Body>
              <Text />
              <H2>{this.state.Code_Value}</H2>
              <Text />
              <Barcode width={(Device.isPhone) ? 3 : 4} value={this.state.Code_Value} format="CODE128" />
              <Text />
              <Text />
              <QRCode
                size={qrSize}
                value={this.state.Code_Value}
              />
            </Body>
          </Content>

        </Container>
      );
    }
  }
}