// 車位共享畫面

import React, { Component } from 'react';
import { Platform, StyleSheet, View } from 'react-native';
import {
  Container,
  Content,
  Button,
  Text
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import L from 'Ifulife/src/components/Layout';


export default class ParkScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: '車位共享',
      headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
      headerTintColor: 'white',
    }
  };

  protected render() {
    return (
      <Container>
        <Content />

        {<L.IFooter />}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});