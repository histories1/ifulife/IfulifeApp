// 意見提出回覆處理紀錄畫面

import React, { Component } from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';
import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Text,
  Button,
  Icon
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';


export default class ListScreen extends Component<Props> {
  static navigationOptions = {
    title: '案件列表',
    headerTintColor: 'white',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      member: undefined,
      opinion: undefined,
    }
  }


  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        let res = await this.setState({
          member: undefined,
          opinion: undefined,
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let opinions = undefined;
        try {
          member = await ExStorage.getMember(this.props.navigation);
          const api = create({
            baseURL: Config.API_URL,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          });
          opinionRes = await api.get('/opinion/list/' + member.unitId + '/' + member.id);
          // console.log('opinion get list: ', opinionRes.data);
          opinions = opinionRes.data;
          if (opinionRes.status !== 200) {
            let cerr = new Error(opinionRes.errMsg);
            cerr.code = opinionRes.code;
            throw cerr;
          }
        } catch (err) {
          console.warn('reject others: ', err);
        } finally {
          let a = await this.setState({
            member,
            opinions,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
      }
    );
  }


  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <Content>
          <List>
          {this.state.opinions &&
          this.state.opinions.map((item, key) => {
              return <OpinionListItem key={key} item={item} memberId={this.state.member.id} />
            })
          }
          </List>
          </Content>

          {<L.IFooter style={styles.colorOpinion2} />}
        </Container>
      );
    }
  }
}


class OpinionList extends Component {

  render() {
    const { navigation, item, memberId } = this.props;
    return (
      <ListItem
        onPress={() => {
          navigation.navigate('Opinion_Record', {
            memberId,
            opinionId: item.id,
            headerTitle: `案件編號${item.opinion_no}`
          });
        }}
      >
        <Left>
          <Text>{item.typeLabel}</Text>
        </Left>
        <Body style={{minWidth:'50%'}}>
          <Text>{item.opinion_no}</Text>
          <Text note>{item.initDateTime}</Text>
          <Text note style={{position:'absolute', top:5, right:10}}>{item.statusLabel}</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    )
  }
}
const OpinionListItem = withNavigation(OpinionList);