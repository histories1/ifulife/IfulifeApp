// 意見提出畫面

import React, { Component } from 'react';
import { Alert } from 'react-native';
import {
  Root,
  Container,
  Content,
  Text,
  Item,
  Picker,
  Textarea,
  Button,
  Input,
  View
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import ImagePicker from 'react-native-image-picker';
import ResponsiveImage from 'react-native-responsive-image';
import DeviceInfo from 'react-native-device-info';
import Device from 'react-native-detect-device';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

const ImagePickerOption = {
  title: "上傳相片",
  cancelButtonTitle: "取消",
  takePhotoButtonTitle: "拍照後直接使用",
  chooseFromLibraryButtonTitle: "從相簿選擇",
  mediaType: "photo",
  maxWidth: 768,
  maxHeight: 640,
  quality: 0.6,
  permissionDenied: {
    title: "未授權程式存取相機",
    text: "您並未授權應用程式使用您的裝置相機功能，若您打算開啟，請至設定中更改。",
    reTryTitle: "重試",
    okTitle: "確定"
  }
};

export default class OpinionSecreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: '意見提出',
      headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
      headerTintColor: 'white',
      headerRight: (
        // 處理清除登入資訊後回到
        <Button transparent
          onPress={() => {
            navigation.navigate('Opinion_List');
          }}
        >
          <Text>案件列表</Text>
        </Button>
      )
    }
  };

  constructor(props) {
    super(props)

    // 處理偵測裝置資訊
    const brand = DeviceInfo.getBrand();
    // 程式發佈版本號
    const buildNumber = DeviceInfo.getBuildNumber();
    // 使用電信商
    const carrier = DeviceInfo.getCarrier();
    // 裝置識別
    const deviceId = DeviceInfo.getDeviceId();
    // 使用語系
    const deviceLocale = DeviceInfo.getDeviceLocale();
    // A string that uniquely identifies this build.
    const fingerprint = DeviceInfo.getFingerprint();
    // Gets the application human readable version.
    const readableVersion = DeviceInfo.getReadableVersion();

    this.state = {
      isLoading: true,
      types: [],
      isApp: false,
      selctedType: undefined,
      appDeviceValue: deviceId,
      appDeviceExt: {
        brand,
        buildNumber,
        carrier,
        deviceLocale,
        fingerprint,
        readableVersion
      },
      textarea: '',
      photoSource: null
    }
  }

  onTypeChange(value: string) {
    if( value === '' ){
      value = undefined;
    }
    this.setState({
      isApp: (value == '52') ? true : false,
      selctedType: value,
      errmsgSelectedType: undefined
    });
  }

  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        let res = await this.setState({
          member: undefined,
          selctedType: undefined,
          errmsgSelectedType: undefined,
          textarea: '',
          errmsgTextarea: undefined,
          photoSource: null,
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let types = undefined;
        const api = create({
          baseURL: Config.API_URL,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        });
        try {
          member = await ExStorage.getMember(this.props.navigation);
          const typesRes = await api.get('/opinion/types');
          console.log('opinion get types: ',typesRes.data);
          if (typesRes.status !== 200) {
            let cerr = new Error(typesRes.errMsg);
            cerr.code = typesRes.code;
            throw cerr;
          }

          if( Device.isAndroid ){
            types = [{label:'請選擇', value:''}, ...typesRes.data];
          }else{
            types = typesRes.data;
          }
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          this.setState({
            member,
            types,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
      }
    );
  }


  async formPost() {
    let formData = new FormData();
    formData.append('memberId', this.state.member.id);
    formData.append('selectedType', this.state.selctedType);
    formData.append('appDeviceValue', this.state.appDeviceValue);
    if (this.state.selctedType == '52' ) {
      formData.append('appDeviceInfo', JSON.stringify(this.state.appDeviceExt) );
    }
    formData.append('textarea', this.state.textarea);
    // const { uri, type, data, fileName, fileSize, width, height} = this.state.photoData;
    if (this.state.photoData ){
      formData.append('photo', {
        uri: this.state.photoData.uri,
        type: this.state.photoData.type,
        name: this.state.photoData.fileName
      });
    }
    // console.log('formData: ',formData);

    try {
      // 呼叫apisauce
      const api = create({
        baseURL: Config.API_URL,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
        }
      });
      api.post('/opinion/form', formData)
      .then((resp) => {
        if (resp.status == 200) {
          // console.log('response data:', resp.data );
          Alert.alert(
            '案件編號' + resp.data.opinionNo,
            resp.data.msg,
            [
              { text: '前往', onPress: () => { this.props.navigation.navigate('Opinion_Record'); } },
            ],
            { cancelable: false },
          );
        } else {
          let cerr = new Error(resp.errMsg);
          cerr.code = resp.code;
          throw cerr;
        }
      }).done(ret => {
        console.log('result: ', ret);
        // 提示成功 => 引導到提出紀錄畫面
      });
    } catch (err) {
      console.warn('opinion posted error: ', err);
    }

  }


  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Root>
        <Container>
          <Content padder>

            <Text note>親愛的住戶您好~</Text>
            <Text note>社區好品質，社區好維護，</Text>
            <Text note>良好的環境你我一同守護，</Text>
            <Text note>對我們的社區有任何建議，</Text>
            <Text note>請盡快提供給管理室了解處理，</Text>
            <Text note>謝謝!</Text>

            <Text />

            <Text>意見類別:</Text>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<FontAwesome name="arrow-down" size={30} color="#bfc6ea" />}
                style={{ width: undefined }}
                placeholder="請選擇"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selctedType}
                onValueChange={this.onTypeChange.bind(this)}
              >
                {
                  this.state.types &&
                  this.state.types.map(function(type, key){
                    return (
                      <Picker.Item key={key} label={type.label} value={type.id} />
                    )
                  })
                }
              </Picker>
            </Item>
            {this.state.errmsgSelectedType &&
              <Text note style={styles.inputErrMsg}>{this.state.errmsgSelectedType}</Text>
            }

            {
              this.state.isApp &&
              (
                <Item>
                  <OcticonsIcon name="device-mobile" />
                    <Input
                      placeholder="填寫您的裝置型號"
                      value={this.state.appDeviceValue}
                      onChangeText={(appDeviceValue) => this.setState({ appDeviceValue })}
                    />
                </Item>
              )
            }

            <Text />

            <Text>意見說明:</Text>
            <Textarea bordered rowSpan={Device.isTablet ? 10 : 5}
              placeholder="請詳細說明 人?事?時?地?物? 如有照片請拍照上傳，謝謝您!"
                onChangeText={(textarea) => this.setState({ textarea, errmsgTextarea:undefined })}
            />
            {this.state.errmsgTextarea &&
                <Text note style={styles.inputErrMsg}>{this.state.errmsgTextarea}</Text>
            }

            <Text />

            <Button block info
              onPress={() => {
                ImagePicker.showImagePicker(ImagePickerOption, (response) => {
                  // console.log('相片資訊 :', response);
                  if (response.didCancel) {
                    console.log('取消上傳相片..');
                  } else if (response.error) {
                    console.log('選擇相簿發生錯誤 訊息: ', response.error);
                  } else {
                    // const source = { uri: response.uri };
                    // You can also display the image using data:
                    const source = { uri: 'data:image/jpeg;base64,' + response.data };
                    this.setState({
                      photoData: response,
                      photoPreview: source,
                    });
                  }
                });
              }}
            >
              <Text>上傳照片</Text>
            </Button>

              {this.state.photoPreview &&
              <View style={{alignItems: 'center',}}>
                <ResponsiveImage
                  initWidth={Device.isPhone ? (Device.width - 80) : 480}
                  initHeight={Device.isPhone ? 200 : 320}
                  source={this.state.photoPreview}
                  // style={{ justifyContent: "space-around" }}
                  resizeMode="contain"
                />
              </View>
            }

            <Text />

            <Button block
              onPress = {() => {
                if(!this.state.selctedType){
                  this.setState({ errmsgSelectedType:'您必須選擇意見類別！'});
                  return;
                } else if (!this.state.textarea){
                  this.setState({ errmsgTextarea: '您必須填寫意見內容！' });
                  return ;
                }

                this.formPost();
              }}
            >
              <Text>送出</Text>
            </Button>

          </Content>
          {<L.IFooter style={styles.colorOpinion2} />}
        </Container>
        </Root>
      );
    }
  }
}