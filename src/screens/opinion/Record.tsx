// 意見提出回覆處理紀錄畫面

import React, { Component } from 'react';
import {
  Container,
  Content,
  Card,
  CardItem,
  Body,
  H3,
  Text
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';


export default class OpinionRecordScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: (navigation.getParam('headerTitle')) ? navigation.getParam('headerTitle') : '',
      headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
      headerTintColor: 'white',
    }
  };


  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      opinionRecord: undefined
    }
  }


  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        let res = await this.setState({
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let opinionRecord= undefined;

        try {
          const api = create({
            baseURL: Config.API_URL,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          });
          opinionRes = await api.get('/opinion/record/' + this.props.navigation.getParam('opinionId'));
          opinionRecord = opinionRes.data;
          // console.log('opinion with replies: ', opinionRecord);
          // return ;
          if (opinionRes.status !== 200) {
            let cerr = new Error(opinionRes.errMsg);
            cerr.code = opinionRes.code;
            throw cerr;
          }
        } catch (err) {
          console.warn('reject others: ', err);
        } finally {
          await this.setState({
            opinionRecord,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
      }
    );
  }


  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <Content>

            <Card style={{ minHeight: 50 }}>
              <CardItem>
                <Body>
                  <H3>{this.state.opinionRecord.typeLabel}</H3>
                  <Text>{this.state.opinionRecord.opinion}</Text>
                  <Text note>{this.state.opinionRecord.opinion_date}</Text>
                </Body>
              </CardItem>
            </Card>

            <Content padder>
            {!this.state.opinionRecord.replies && <OpinionRecordReply item="empty" />}
            {this.state.opinionRecord.replies &&
              this.state.opinionRecord.replies.map(function(item, key){
                return (
                  <OpinionRecordReply key={key} item={item} />
                )
              })
            }
            </Content>

          </Content>

          {<L.IFooter style={styles.colorOpinion2} />}
        </Container>
      );
    }
  }
}


class OpinionRecordReply extends Component<Props> {
  render() {
    const { item } = this.props;

    if( item == "empty" ){
      return (
        <Card style={{ minHeight: 20 }}>
          <CardItem header>
            <H3>目前對此意見，尚未有任何回覆。</H3>
          </CardItem>
        </Card>
      )
    }else{
      return (
        <Card style={{ minHeight: 40 }}>
          <CardItem header>
            <H3>由{item.managerName}於{item.replyDateTime}回覆</H3>
          </CardItem>
          <CardItem>
            <Body style={{ minWidth: '55%' }}>
              <Text>{item.reply}</Text>
            </Body>
          </CardItem>
        </Card>
      )
    }
  }
}