// 公設預約

import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import {
  Left,
  Right,
  Body,
  H2,
  H3,
  Text,
  Button,
  Card,
  CardItem,
} from 'native-base';

import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

class UtilitiesCards extends Component {

  static navigationOptions = {
    headerStyle: styles.colorUtilities1,
  };

  protected render() {
    return (
      <Card>
        <CardItem header>
          <Left>
            <H2>健身房</H2>
          </Left>
          <Right>
            <Text>目前狀態：可使用</Text>
          </Right>
        </CardItem>
        <CardItem bordered>
          <Body>
            <H3>介紹內容</H3>
            <Text>顯示照片</Text>
            <Text>所在位置: B棟 B1樓</Text>
            <Text>人數上限: 10人</Text>
            <Text>使用點數: 20點/次</Text>
          </Body>
        </CardItem>
        <CardItem>
          <Left>
            <Button small bordered info
              onPress={() => {
                this.props.navigation.navigate('Utilities_Record');
              }}
            >
              <Text>預約情況</Text>
            </Button>
          </Left>
          <Right>
            <Button small bordered primary
              onPress={() => {
                this.props.navigation.navigate('Utilities_Policy', {
                  title: '健身房',
                  titlePolicy: '健身房管理規範'
                });
            }}>
              <Text>我要預約</Text>
            </Button>
          </Right>
        </CardItem>
      </Card>
    );
  }
}
const ListCard = withNavigation(UtilitiesCards);
export default ListCard