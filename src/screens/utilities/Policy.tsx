// 公設預約顯示預約規範

import React, { Component } from 'react';
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Text,
  Button,
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';


export default class UtilityPolicy extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: styles.colorUtility1,
      title: navigation.getParam('title')
    }
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      utility: undefined,
      utility_policy: undefined,
      dates: []
    }

    this.setDates = this.setDates.bind(this);
  }


  async componentDidMount() {
    // this.props.navigation.addListener(
    //   'willFocus', async (payload) => {
    //     let res = await this.setState({
    //       utility: undefined,
    //       isLoading: true
    //     });
    //     // console.log('will focus', payload.state.param.member)
    //   }
    // );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
    //     let member = undefined;
    //     let typesRes = undefined;
    //     try {
    //       member = await ExStorage.getMember(this.props.navigation);
    //       const api = create({
    //         baseURL: Config.API_URL,
    //         headers: {
    //           'Accept': 'application/json',
    //           'Content-Type': 'application/json'
    //         }
    //       });
    //     } catch (err) {
    //       if (!err) {
    //         member = err;
    //       } else {
    //         console.warn('reject others: ', err);
    //       }
    //     } finally {
          let a = await this.setState({
    //         member,
            isLoading: false,
          });
        }
    //     // console.log('did focus', this.state.member)
    );
  }


  protected setDates() {
    this.setState({ dates });
  }


  protected render() {
    const { dates } = this.state;
    const theme = {
      calendarMonth: {
        title: {
          color: "red"
        }
      }
    };

    let content = undefined;
    if (this.state.isLoading) {
      content = <L.PageLoading />
    } else {
      content = (
          <Content padder>

            <Text>畫面顯示公共設施使用規範</Text>
            <Text>畫面顯示公共設施使用規範</Text>
            <Text>畫面顯示公共設施使用規範</Text>
            <Text>畫面顯示公共設施使用規範</Text>
            <Text>畫面顯示公共設施使用規範</Text>
            <Text>畫面顯示公共設施使用規範</Text>

          </Content>
      )
    }

    const title = this.props.navigation.getParam('title');
    const titlePolicy = this.props.navigation.getParam('titlePolicy');

    return (
      <Container>

        <Header>
          <Left>
            <Button transparent
              onPress={() => { this.props.navigation.goBack() }}
            >
              <FontAwesome size={20} name="times-circle-o" color="blue" />
              <Text>關閉</Text>
            </Button>
          </Left>
          <Body>
            <Title>{titlePolicy}</Title>
          </Body>
          <Right>
            <Button transparent
              onPress={() => { this.props.navigation.navigate('Utilities_Booking', {
                title
              }) }}
            >
              <FontAwesome size={20} name="pencil-square-o" color="blue" />
              <Text>預約</Text>
            </Button>
          </Right>
        </Header>
        {content}
      </Container>
    );
  }
}