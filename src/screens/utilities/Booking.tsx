// 公設預約表單畫面

import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Container,
  Content,
  Text,
  Item,
  Picker,
  Textarea,
  Button,
  Input
} from 'native-base';
import { Calendar } from 'react-native-calendars';
import MultiSelect from 'react-native-multiple-select';


import { create } from 'apisauce';
import Config from "react-native-config";
import ExStorage from 'Ifulife/src/components/ExStorage';
import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

global.bookSetDates = [];
global.multipleitems = [{
  id: 1,
  section: '00:00~00:30',
}, {
  id: 2,
  section: '00:30~01:00',
}, {
  id: 3,
  section: '01:00~01:30',
}, {
  id: 4,
  section: '01:30~02:00',
}, {
  id: 5,
  section: '02:00~02:30',
}, {
  id: 6,
  section: '02:30~03:00',
}, {
  id: 7,
  section: '03:00:03:30',
}, {
  id: 8,
  section: '03:30:04:00',
}, {
  id: 9,
  section: '04:00~04:30',
}];

export default class UtilitiesBooking extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerMode: 'screen',
      headerStyle: styles.colorUtilities1,
      headerTintColor: 'white',
      title: navigation.getParam('title')
    }
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      utility: undefined,
      dates: undefined,
      marked: null,
      selectedItems: []
    }

    // 設定多個日期後配置對應時段清單
    this.setDates = this.setDates.bind(this);
    this.onDayPress = this.onDayPress.bind(this);
    this.onDayLongPress = this.onDayLongPress.bind(this);
    this.onSelectedItemsChange = this.onSelectedItemsChange.bind(this);
  }


  async componentDidMount() {
    // this.props.navigation.addListener(
    //   'willFocus', async (payload) => {
    //     let res = await this.setState({
    //       member: undefined,
    //       utility: undefined,
    //       isLoading: true
    //     });
    //     // console.log('will focus', payload.state.param.member)
    //   }
    // );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
    //     let member = undefined;
    //     let typesRes = undefined;
    //     try {
    //       member = await ExStorage.getMember(this.props.navigation);
    //       const api = create({
    //         baseURL: Config.API_URL,
    //         headers: {
    //           'Accept': 'application/json',
    //           'Content-Type': 'application/json'
    //         }
    //       });
    //     } catch (err) {
    //       if (!err) {
    //         member = err;
    //       } else {
    //         console.warn('reject others: ', err);
    //       }
    //     } finally {
          let a = await this.setState({
    //         member,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
    );

    global.bookSetDates = [
      '2019-06-18',
      '2019-06-17',
      '2019-05-28',
      '2019-05-29'
    ];
    this.setDates();
  }


  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };


  protected setDates() {
    var obj = global.bookSetDates.reduce((c, v) => Object.assign(c, {[v]: {selected: true,marked: true}}), {});
    this.setState({ marked : obj});
  }


  /**
   * 選擇日期後可出現對應時段並標示
   * @param date
   */
  protected onDayPress(date) {
    global.bookSetDates.push(date.dateString);
    // console.log('date: ', date.dateString);
    this.setDates();
    return;
    let setdate = { [date.dateString]: { selected: true, marked: true }};
    this.setState({
      marked: [...this.state.marked, setdate]
    });
    console.log(this.state.marked);
  }

  /**
   * 長按取消選擇日期
   * @param date
   */
  protected onDayLongPress(date) {
    global.bookSetDates = global.bookSetDates.filter(e => e !== date.dateString);
    this.setDates();
    return;
    // let temp = this.state.marked;
    // delete temp[date.dateString];
    // this.setState({
    //   marked: temp
    // })
    console.log('long press:', this.state.marked);
  }


  protected render() {
    const { isLoading, selectedItems } = this.state;

    if (isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>

          <Content padder>

            <Text>公設項目介紹區塊</Text>

            <Text>顯示住戶預約點數資訊</Text>

            <Text>選擇日期</Text>
            <Calendar
              onDayPress={this.onDayPress}
              onDayLongPress={this.onDayLongPress}
              current={new Date()}
              minDate={'2019-05-24'}
              onMonthChange={(month) => {
                console.log('month changed', month)}
              }
              disableMonthChange={false}
              firstDay={1}
              hideDayNames={false}
              markedDates={this.state.marked}
              theme={{
                backgroundColor: '#ffffff',
                calendarBackground: '#ffffff',
                textSectionTitleColor: '#b6c1cd',
                selectedDayBackgroundColor: '#00adf5',
                selectedDayTextColor: '#ffffff',
                todayTextColor: '#00adf5',
                dayTextColor: '#2d4150',
                textDisabledColor: '#d9e1e8',
                dotColor: '#00adf5',
                selectedDotColor: '#ffffff',
                arrowColor: 'orange',
                monthTextColor: 'blue',
                textMonthFontWeight: 'bold',
                textDayFontSize: 16,
                textMonthFontSize: 16,
                textDayHeaderFontSize: 16
              }}
            />

            <Text>根據選擇的日期，顯示可預約時段清單</Text>
            {global.bookSetDates &&
            global.bookSetDates.map(function(date, key) {
              return (
                <View key={key}>
                  <Text>{date}</Text>
                  <MultiSelect
                    hideTags
                    items={global.multipleitems}
                    uniqueKey="id"
                    displayKey="section"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.onSelectedItemsChange}
                    selectedItems={selectedItems}
                    selectText="選擇預約時段"
                    searchInputPlaceholderText="快速搜尋..."
                    onChangeInput={(text) => console.log(text)}
                    // altFontFamily="ProximaNova-Light"
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="確定"
                  />
                </View>
              )
            })
            }


            <Text>顯示住戶預約點數資訊</Text>

          </Content>

          {<L.IFooter style={styles.colorUtilities2} />}
        </Container>
      );
    }
  }
}