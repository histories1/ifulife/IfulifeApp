// 行事曆

import React, { Component } from 'react';
import {
  Container,
  Text,
  Tab,
  Tabs,
} from 'native-base';
import ExStorage from 'Ifulife/src/components/ExStorage';
import { create } from 'apisauce';
import Config from "react-native-config";
import moment from "moment";
import ListMode from '../calendar/ListMode';

import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';

export default class CalendarSecreen extends Component {
  static navigationOptions = {
    headerStyle: styles.colorCalendar1,
  };

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      utilities: undefined,
    }
  }


  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        this.setState({
          member: undefined,
          utilities: undefined,
          isLoading: true
        });
      }
    );

    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let utilitiesRes = undefined;
        let dateRange = '2019-06-01_2019-06-30';
        try {
          member = await ExStorage.getMember(this.props.navigation);
          /**
           * @todo 加入判斷機制導向登入提示！
           */
          // console.log(member);
          const api = create({
            baseURL: Config.API_URL,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          });

          utilitiesRes = await api.get('/msg/calendar/' + member.cmtId + '/' + dateRange);
          // console.log('calendar get: ',utilitiesRes.data);
          // return ;
          if (utilitiesRes.status !== 200) {
            let cerr = new Error(utilitiesRes.errMsg);
            cerr.code = utilitiesRes.code;
            throw cerr;
          }
          utilities = utilitiesRes.data;
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          let a = await this.setState({
            member,
            utilities,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.utilities);
      }
    );

  }

  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <ListMode />

          {<L.IFooter style={styles.colorCalendar2} />}
        </Container>
      );
    }
  }
}