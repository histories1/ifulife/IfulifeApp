// 公設預約

import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import {
  Container,
  Content,
} from 'native-base';
import { create } from 'apisauce';
import Config from "react-native-config";
import EStyleSheet from 'react-native-extended-stylesheet';
import ExStorage from 'Ifulife/src/components/ExStorage';
import ListCard from './ListCard';

import L from 'Ifulife/src/components/Layout';
import styles from 'Ifulife/src/constants/newstyles';


export default class UtilitiesScreen extends Component {
  static navigationOptions = {
    title: '公設預約',
    headerStyle: { backgroundColor: EStyleSheet.value('$colorMainBackground') },
    headerTintColor: 'white',
  };


  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      member: undefined,
      utilities: undefined
    }
  }


  async componentDidMount() {
    this.props.navigation.addListener(
      'willFocus', async (payload) => {
        let res = await this.setState({
          member: undefined,
          utilities: undefined,
          isLoading: true
        });
        // console.log('will focus', payload.state.param.member)
      }
    );
    this.props.navigation.addListener(
      'didFocus', async (payload) => {
        let member = undefined;
        let typesRes = undefined;
        try {
          member = await ExStorage.getMember(this.props.navigation);
          const api = create({
            baseURL: Config.API_URL,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          });
        } catch (err) {
          if (!err) {
            member = err;
          } else {
            console.warn('reject others: ', err);
          }
        } finally {
          let a = await this.setState({
            member,
            isLoading: false,
          });
        }
        // console.log('did focus', this.state.member)
      }
    );
  }


  protected render() {
    if (this.state.isLoading) {
      return (
        <L.PageLoading />
      )
    } else {
      return (
        <Container>
          <Content>
          {/* {
              this.state.utilities && this.state.utilities.map(function(item, key){
                return (
                  <UtilitiesList key={key} utility={item} />
                )
              })
          } */}
            <ListCard />
          </Content>

          {<L.IFooter style={styles.colorUtilities2} />}
        </Container>
      );
    }
  }
}