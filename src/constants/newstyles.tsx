import EStyleSheet from 'react-native-extended-stylesheet';
import { Platform } from 'react-native';
/* @NOTE https://github.com/ChildishDanbino/react-native-detect-device */
import Device from 'react-native-detect-device';

const { pixelDensity, width, height, isIos, isAndroid, isPhone, isTablet, isIphoneX } = Device;

// local variable
EStyleSheet.build({
  $textColor: '#0275d8',
  // 配置版面色系
  $colorMainText: '#00629A',
  // 藍色
  $colorMainBackground: '#00629A',
  // 黃色
  $colorSubem: '#FAC000',

  // 定義預設字體尺寸
  // $rem: width > 500 ? 20 : 16
});

// 根據指定平台配置不同樣式作法
// ...Platform.select({
//   ios: {
//   },
//   android: {
//   }
// })

// 根據手機還是平板判斷作法
// 原本為(global.isPad) ? '': '',改用 (global.isPad) ? '': '',

const styles = EStyleSheet.create({
  flex : { flex: 1},
  center : { flex: 1, alignItems: 'center', justifyContent: 'center' },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainbackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: Device.width,
    height: Device.height
  },
  logoBock: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '$colorSubem',
    borderRadius: 60,
    backgroundColor: '$colorSubem',
    width: Device.width-50,
    marginLeft: 20,
    marginVertical: 15,
    ...Platform.select({
      ios: {
        height: 48,
      },
      android: {
        height: 45,
      }
    })
  },
  logoSelf: {
    position: 'absolute',
    top: -15,
    left: -15
  },
  logoTitle: {
    fontSize: 22,
    color: '$colorMainText',
    marginLeft: 70,
    ...Platform.select({
      ios: {
        fontWeight: 'bold',
        marginTop: 10
      },
      android: {
        marginTop:5
      }
    })
  },
  logoBtn: {
    position: 'absolute',
    backgroundColor: '#00629A',
    top: 8,
    right: 10,
    width: 100,
    height:30,
  },
  logoBtnTxt: {
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 14,
    ...Platform.select({
      ios: {
        fontWeight: 'bold',
      },
      android: {
      }
    })
  },
  logoBtnIcon: {
    ...Platform.select({
      ios: {
        position: 'absolute',
        right: 0
      },
    })
  },
  splinkBlock: {
    width: Device.width - 50,
    height: 30,
    marginTop:-5,
    alignItems: 'flex-end',
  },
  newsBlock: {
    width: '90%',
    height: '90%',
    marginHorizontal: '5%',
    marginTop: -5,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  newsHeader: {
    borderBottomColor: '#DDDDDD',
    borderBottomWidth: 0.7,
  },
  newsIcon: {
    marginLeft:20,
    marginRight: 10,
  },
  newsTitle: {
    color: '$colorMainText',
    fontSize: 16
  },
  newsListBtn: {
    alignSelf: 'flex-end',
    borderColor: '#979797',
    borderRadius: 7,
    marginRight: 10,
    width: 100,
    height: 26,
    flexDirection: 'row',
  },
  newsBadges:{
    ...Platform.select({
      ios: {
        top: 20
      },
      android: {
        top: 13,
        left: 5
      }
    })
  },
  newsGoBtn: {
    alignSelf: 'flex-end',
    alignContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    flexDirection: 'column',
    borderColor: '#979797',
    borderWidth: 1,
    borderRadius: 10,
    marginRight: 10,
    paddingHorizontal: 0,
  },
  menuItem: {
    backgroundColor: 'white',
    width:'85%',
    borderRadius: 10,
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    ...Platform.select({
      ios: {
        height: '80%',
        marginBottom: '10%',
      },
      android: {
        height: '80%',
        marginBottom: '5%',
      }
    }),
  },
  menuLeft:{
    marginLeft: '10%',
    marginRight: '5%',
  },
  menuRight: {
    marginRight: '10%',
    marginLeft: '5%',
  },
  menuIcon: {
    marginLeft: 15,
    marginRight:10
  },
  menuBadge: {
    ...Platform.select({
      ios: {
        top: 0,
        left: 5
      },
      android: {
        top: 0,
        left: 0
      }
    })
  },
  splash: {
    width: Device.width,
    height: Device.height
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  listViewStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  PageLoading: {
    position: 'absolute',
    top: Device.height / 2,
    left: Device.width / 2
  },
  /**
   * 最新消息列表
   */
  tabBackground: {
    backgroundColor: 'white',
  },
  textStyle: {
    color: 'black'
  },
  activeTextStyle: {
    color: '$colorMainBackground'
  },
  /**
   * SwiperBanner
   */
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  /**
   * 月曆列表模式樣式
   */
  item: {
    padding: 20,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: '#e8ecf0',
    flexDirection: 'row'
  },
  itemHourText: {
    color: 'black'
  },
  itemDurationText: {
    color: 'grey',
    fontSize: 12,
    marginTop: 4,
    marginLeft: 4
  },
  itemTitleText: {
    width:'60%',
    color: 'black',
    marginLeft: 16,
    fontWeight: 'bold',
    fontSize: 16
  },
  itemButtonContainer: {
    flex: 1,
    alignItems: 'flex-end'
  },
  emptyItem: {
    paddingLeft: 20,
    height: 52,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#e8ecf0'
  },
  emptyItemText: {
    color: '#79838a',
    fontSize: 14
  },
  /**
   * 瓦斯抄表
   */
  gasInput: {
    fontSize: 30,
    fontWeight: 'bold',
    width: 200,
    height: 60
  },
  /**
   *
   */
  inputErrMsg: {
    color: 'red',
    marginTop: 5,
    marginLeft: 20
  }
});
export default styles;