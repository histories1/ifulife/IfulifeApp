import React from 'react'
import Svg, { Path } from 'react-native-svg'

const SvgComponent = props => (
  <Svg width="1em" height="1em" viewBox="0 0 9 11" {...props}>
    <Path
      d="M7.81 6.284c-.703-.5-1.78-.234-2.451-1.124 0 0-.25-.28.312-.967.562-.687.609-1.78.468-2.623C6 .727 5.203.165 4.375.165c-.827 0-1.623.562-1.763 1.405-.141.843-.094 1.936.468 2.623.562.686.312.967.312.967-.671.89-1.748.625-2.45 1.124-.703.5-.532 1.64-.532 1.64 3.187 0 1.097 2.13 3.965 2.13 2.87 0 .778-2.13 3.965-2.13 0 0 .172-1.14-.53-1.64z"
      fill="#FFF"
      fillRule="evenodd"
    />
  </Svg>
)

export default SvgComponent
