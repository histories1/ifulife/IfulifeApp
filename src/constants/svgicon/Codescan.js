import React from 'react'
import Svg, { G, Path } from 'react-native-svg'

const SvgComponent = props => (
  <Svg width="1em" height="1em" viewBox="0 0 9 10" {...props}>
    <G fill="#FFF" fillRule="evenodd">
      <Path d="M5.115 2.056a1.327 1.327 0 1 1-2.654 0 1.327 1.327 0 0 1 2.654 0zM2.822 4.656a1.13 1.13 0 1 1-2.262 0 1.13 1.13 0 0 1 2.262 0zM3.64 7.57a1.039 1.039 0 1 1-2.077 0 1.039 1.039 0 0 1 2.078 0zM6.09 8.608a.883.883 0 1 1-1.766 0 .883.883 0 0 1 1.765 0zM8.171 7.57a.763.763 0 1 1-1.527 0 .763.763 0 0 1 1.527 0zM9.003 5.671a.644.644 0 1 1-1.288 0 .644.644 0 0 1 1.288 0zM8.597 3.823a.47.47 0 1 1-.94 0 .47.47 0 0 1 .94 0zM7.414 2.525a.36.36 0 1 1-.719 0 .36.36 0 0 1 .72 0z" />
    </G>
  </Svg>
)

export default SvgComponent
