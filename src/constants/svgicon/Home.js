import React from 'react'
import Svg, { Path } from 'react-native-svg'

const SvgComponent = props => (
  <Svg width="1em" height="1em" viewBox="0 0 9 9" {...props}>
    <Path
      d="M7.576 4.988a.752.752 0 0 1-.505-.194L4.164 2.187 1.258 4.794A.756.756 0 1 1 .248 3.67L4.163.155l1.257 1.127V.66H6.76v1.823l1.322 1.186a.756.756 0 0 1-.506 1.319zm-3.412-1.66L.752 6.451v1.102c0 .281.229.51.51.51h1.82V6.388h2.165v1.676h1.82c.28 0 .51-.229.51-.51V6.452L4.163 3.327z"
      fill="#FFF"
      fillRule="evenodd"
    />
  </Svg>
)

export default SvgComponent
