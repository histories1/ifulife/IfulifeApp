import React from 'react'
import Svg, { Defs, LinearGradient, Stop, Path } from 'react-native-svg'

const SvgComponent = props => (
  <Svg width="1em" height="1em" viewBox="0 0 26 24" {...props}>
    <Defs>
      <LinearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="prefix__a">
        <Stop stopColor="#F78B00" offset="0%" />
        <Stop stopColor="#FAC000" offset="100%" />
      </LinearGradient>
    </Defs>
    <Path
      d="M24.06 24H2.18c-.465 0-.842-.48-.842-1.074V6.282h23.563v16.644c0 .593-.377 1.074-.842 1.074zm1.492-18.576H.448C.201 5.424 0 5.18 0 4.882V1.085C0 .485.401 0 .897 0h24.206C25.6 0 26 .486 26 1.085v3.797c0 .3-.2.542-.448.542zm-9.51 3.389h-5.798c-.458 0-.829.486-.829 1.085s.371 1.085.829 1.085h5.799c.457 0 .828-.486.828-1.085 0-.6-.37-1.085-.828-1.085z"
      fill="url(#prefix__a)"
      fillRule="nonzero"
    />
  </Svg>
)

export default SvgComponent
